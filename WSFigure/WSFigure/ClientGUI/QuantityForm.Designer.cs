﻿namespace WSFigure
{
    partial class QuantityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Nup_Quantity = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.L_MaxValue = new System.Windows.Forms.Label();
            this.Btn_Submit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Nup_Quantity)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Quantity:";
            // 
            // Nup_Quantity
            // 
            this.Nup_Quantity.Location = new System.Drawing.Point(62, 56);
            this.Nup_Quantity.Name = "Nup_Quantity";
            this.Nup_Quantity.Size = new System.Drawing.Size(120, 20);
            this.Nup_Quantity.TabIndex = 11;
            this.Nup_Quantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Input your Quantity:";
            // 
            // L_MaxValue
            // 
            this.L_MaxValue.AutoSize = true;
            this.L_MaxValue.ForeColor = System.Drawing.Color.Red;
            this.L_MaxValue.Location = new System.Drawing.Point(72, 31);
            this.L_MaxValue.Name = "L_MaxValue";
            this.L_MaxValue.Size = new System.Drawing.Size(47, 13);
            this.L_MaxValue.TabIndex = 13;
            this.L_MaxValue.Text = "(Stock: )";
            // 
            // Btn_Submit
            // 
            this.Btn_Submit.Location = new System.Drawing.Point(62, 87);
            this.Btn_Submit.Name = "Btn_Submit";
            this.Btn_Submit.Size = new System.Drawing.Size(75, 23);
            this.Btn_Submit.TabIndex = 14;
            this.Btn_Submit.Text = "OK";
            this.Btn_Submit.UseVisualStyleBackColor = true;
            this.Btn_Submit.Click += new System.EventHandler(this.Btn_Submit_Click);
            // 
            // QuantityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(189, 122);
            this.Controls.Add(this.Btn_Submit);
            this.Controls.Add(this.L_MaxValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Nup_Quantity);
            this.Controls.Add(this.label1);
            this.Name = "QuantityForm";
            this.Text = "Quantity";
            this.Load += new System.EventHandler(this.QuantityForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Nup_Quantity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown Nup_Quantity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label L_MaxValue;
        private System.Windows.Forms.Button Btn_Submit;
    }
}