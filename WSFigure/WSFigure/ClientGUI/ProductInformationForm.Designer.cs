﻿namespace WSFigure
{
    partial class ProductInformationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtBox_Name = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtBox_Manufacturer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtBox_Scale = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PBox_Image = new System.Windows.Forms.PictureBox();
            this.RTxtBox_Info = new System.Windows.Forms.RichTextBox();
            this.Btn_Return = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBox_Image)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Name:";
            // 
            // TxtBox_Name
            // 
            this.TxtBox_Name.Location = new System.Drawing.Point(85, 32);
            this.TxtBox_Name.Name = "TxtBox_Name";
            this.TxtBox_Name.ReadOnly = true;
            this.TxtBox_Name.Size = new System.Drawing.Size(334, 20);
            this.TxtBox_Name.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtBox_Manufacturer);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtBox_Scale);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtBox_Name);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(425, 120);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Details";
            // 
            // TxtBox_Manufacturer
            // 
            this.TxtBox_Manufacturer.Location = new System.Drawing.Point(85, 84);
            this.TxtBox_Manufacturer.Name = "TxtBox_Manufacturer";
            this.TxtBox_Manufacturer.ReadOnly = true;
            this.TxtBox_Manufacturer.Size = new System.Drawing.Size(334, 20);
            this.TxtBox_Manufacturer.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Manufacturer:";
            // 
            // TxtBox_Scale
            // 
            this.TxtBox_Scale.Location = new System.Drawing.Point(85, 58);
            this.TxtBox_Scale.Name = "TxtBox_Scale";
            this.TxtBox_Scale.ReadOnly = true;
            this.TxtBox_Scale.Size = new System.Drawing.Size(334, 20);
            this.TxtBox_Scale.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Scale:";
            // 
            // PBox_Image
            // 
            this.PBox_Image.Location = new System.Drawing.Point(443, 12);
            this.PBox_Image.Name = "PBox_Image";
            this.PBox_Image.Size = new System.Drawing.Size(376, 405);
            this.PBox_Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBox_Image.TabIndex = 15;
            this.PBox_Image.TabStop = false;
            // 
            // RTxtBox_Info
            // 
            this.RTxtBox_Info.Location = new System.Drawing.Point(12, 138);
            this.RTxtBox_Info.Name = "RTxtBox_Info";
            this.RTxtBox_Info.ReadOnly = true;
            this.RTxtBox_Info.Size = new System.Drawing.Size(419, 256);
            this.RTxtBox_Info.TabIndex = 16;
            this.RTxtBox_Info.Text = "";
            // 
            // Btn_Return
            // 
            this.Btn_Return.Location = new System.Drawing.Point(12, 400);
            this.Btn_Return.Name = "Btn_Return";
            this.Btn_Return.Size = new System.Drawing.Size(75, 23);
            this.Btn_Return.TabIndex = 17;
            this.Btn_Return.Text = "Return";
            this.Btn_Return.UseVisualStyleBackColor = true;
            this.Btn_Return.Click += new System.EventHandler(this.Btn_Return_Click);
            // 
            // ProductInformationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 434);
            this.Controls.Add(this.Btn_Return);
            this.Controls.Add(this.RTxtBox_Info);
            this.Controls.Add(this.PBox_Image);
            this.Controls.Add(this.groupBox1);
            this.Name = "ProductInformationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Figure Information";
            this.Load += new System.EventHandler(this.ProductInformationForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBox_Image)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtBox_Name;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TxtBox_Scale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtBox_Manufacturer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox PBox_Image;
        private System.Windows.Forms.RichTextBox RTxtBox_Info;
        private System.Windows.Forms.Button Btn_Return;
    }
}