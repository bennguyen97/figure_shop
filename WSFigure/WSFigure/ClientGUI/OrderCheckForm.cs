﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WSFigure
{
    public partial class OrderCheckForm : Form
    {
        private int Id_Order = 0;
        private int Total_Price = 0;
        private int Max_Quantity_Product = 0;
        public OrderCheckForm()
        {
            InitializeComponent();
        }

        public OrderCheckForm(int Id_Order, int Total_Price)
        {
            this.Id_Order = Id_Order;
            this.Total_Price = Total_Price;
            InitializeComponent();
        }

        private void OrderCheckForm_Load(object sender, EventArgs e)
        { 
            DisplayProductListOrder(Id_Order);
            TxtBox_Total.Text = Total_Price.ToString();
        }  

        private void Btn_Delete_Click(object sender, EventArgs e)
        {
           //get invisible column
           int product_id = int.Parse(Dgv_ProductDetailed.SelectedRows[0].Cells["Code"].Value.ToString());
           string message = "Are you sure to delete this row?";
           string title = "Confirm";
           MessageBoxButtons buttons = MessageBoxButtons.YesNo;
           DialogResult log_result = MessageBox.Show(message, title, buttons);
           if (log_result == DialogResult.Yes)
           {
               DeleteProductListOrder(Id_Order, product_id);
           }
           else
           {
               // Do nothing
           }
        }

        private void Btn_editQuantity_Click(object sender, EventArgs e)
        {
            int product_id = int.Parse(Dgv_ProductDetailed.SelectedRows[0].Cells["Code"].Value.ToString());
            int price = int.Parse(Dgv_ProductDetailed.SelectedRows[0].Cells["Price"].Value.ToString());

            Max_Quantity_Product = int.Parse(Dgv_ProductDetailed.SelectedRows[0].Cells["Max_Quantity"].Value.ToString());
            QuantityForm quantity_form = new QuantityForm(Max_Quantity_Product);
            quantity_form.ShowDialog();

            int previous_quantity = int.Parse(Dgv_ProductDetailed.SelectedRows[0].Cells["Max_Quantity"].Value.ToString());
            int lastest_quantity = quantity_form.Quantity;

            if (lastest_quantity == 0)
            {
                string message = "You are changing a product quantity downto 0, which means you do not want to buy this anymore. Continue?";
                string title = "Confirm";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult log_result = MessageBox.Show(message, title, buttons);
                if (log_result == DialogResult.Yes)
                {
                    DeleteProductListOrder(Id_Order, product_id);
                }
                else
                {
                    // Do nothing
                }
            }
            else if (previous_quantity != lastest_quantity)
            {
                UpdateProductListOrder(Id_Order, product_id, lastest_quantity, lastest_quantity * price);
            }
            else
            {
                //Do nothing
            }
        }

        private void Btn_Confirm_Click(object sender, EventArgs e)
        {
            Total_Price = int.Parse(TxtBox_Total.Text);
            CustomerInformationForm cus_form = new CustomerInformationForm(Id_Order, Total_Price);
            cus_form.ShowDialog();
        }

        private void LoadProductFromListOrder(List<Receipt> receipts)
        {
            Dgv_ProductDetailed.BeginInvoke(new MethodInvoker(delegate
            {
                Dgv_ProductDetailed.DataSource = receipts;
                Dgv_ProductDetailed.Refresh();

                Dgv_ProductDetailed.Columns["Code"].HeaderText = "Code";
                Dgv_ProductDetailed.Columns["Code"].ReadOnly = true;
                Dgv_ProductDetailed.Columns["Code"].Width = 30;

                Dgv_ProductDetailed.Columns["Name"].HeaderText = "Product Name";
                Dgv_ProductDetailed.Columns["Name"].ReadOnly = true;
                Dgv_ProductDetailed.Columns["Name"].Width = 200;

                Dgv_ProductDetailed.Columns["Price"].HeaderText = "Price";
                Dgv_ProductDetailed.Columns["Price"].ReadOnly = true;
                Dgv_ProductDetailed.Columns["Price"].Width = 70;

                Dgv_ProductDetailed.Columns["Quantity"].HeaderText = "Quantity";
                Dgv_ProductDetailed.Columns["Quantity"].ReadOnly = true;
                Dgv_ProductDetailed.Columns["Quantity"].Width = 50;

                Dgv_ProductDetailed.Columns["Max_Quantity"].HeaderText = "Max_Quantity";
                Dgv_ProductDetailed.Columns["Max_Quantity"].ReadOnly = true;
                Dgv_ProductDetailed.Columns["Max_Quantity"].Width = 50;

                Dgv_ProductDetailed.Columns["Code"].Visible = false;
                Dgv_ProductDetailed.Columns["Max_Quantity"].Visible = false;

                TxtBox_Total.Text = Sum_Total().ToString();
            }));
        }

        private void DisplayProductListOrder(int ordersId)
        {
            string url = "http://www.figuresoap.somee.com/api/FigureService/ShowOrderProductList?OrdersId=" + ordersId;
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Receipt> receipts = JsonConvert.DeserializeObject<List<Receipt>>(json);
            LoadProductFromListOrder(receipts);
        }

        private void DeleteProductListOrder(int orderId, int productId)
        {
            List<int> dataList = new List<int>();
            dataList.Add(orderId);
            dataList.Add(productId);
            var data = JsonConvert.SerializeObject(dataList);
            string url = "http://www.figuresoap.somee.com/api/FigureService/DeleteDetailedOrder?orderId=" + orderId +
                "&productId=" + productId;
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            bool result = bool.Parse(client.UploadString(new Uri(url), data));
            if (result)
            {
                MessageBox.Show("Delete successfully!", "Notification");
                DisplayProductListOrder(Id_Order);
            }
            else
            {
                MessageBox.Show("Failed to delete! Please try again!", "Notification");
            }
        }

        private void UpdateProductListOrder(int orderId, int productId, int quantity, int totalPrice)
        {
            List<int> dataList = new List<int>();
            dataList.Add(orderId);
            dataList.Add(productId);
            dataList.Add(quantity);
            dataList.Add(totalPrice);
            var data = JsonConvert.SerializeObject(dataList);
            string url = "http://www.figuresoap.somee.com/api/FigureService/UpdateDetailedOrder?orderId=" + orderId +
                "&productId=" + productId + "&quantity=" + quantity + "&totalPrice=" + totalPrice;
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            bool result = bool.Parse(client.UploadString(new Uri(url), data));
            if (result)
            {
                MessageBox.Show("Update successfully!", "Notification");
                DisplayProductListOrder(Id_Order);
            }
            else
            {
                MessageBox.Show("Failed to update! Please try again!", "Notification");
            }
        }

        private int Sum_Total()
        {
            int sum = 0;
            foreach(DataGridViewRow row in Dgv_ProductDetailed.Rows)
            {
                int n = row.Index;
                sum += int.Parse(Dgv_ProductDetailed.Rows[n].Cells[2].Value.ToString()) *
                    int.Parse(Dgv_ProductDetailed.Rows[n].Cells[3].Value.ToString());
            }
            return sum;
        }
    }
}
