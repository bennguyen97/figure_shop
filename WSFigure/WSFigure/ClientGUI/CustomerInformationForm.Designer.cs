﻿namespace WSFigure
{
    partial class CustomerInformationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TxtBox_AddCusAddress = new System.Windows.Forms.TextBox();
            this.TxtBox_AddCusName = new System.Windows.Forms.TextBox();
            this.Btn_Submit = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBox_AddCusPhone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TxtBox_AddCusAddress);
            this.groupBox3.Controls.Add(this.TxtBox_AddCusName);
            this.groupBox3.Controls.Add(this.Btn_Submit);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.TxtBox_AddCusPhone);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(312, 139);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Customer Information";
            // 
            // TxtBox_AddCusAddress
            // 
            this.TxtBox_AddCusAddress.Location = new System.Drawing.Point(73, 45);
            this.TxtBox_AddCusAddress.Name = "TxtBox_AddCusAddress";
            this.TxtBox_AddCusAddress.Size = new System.Drawing.Size(221, 20);
            this.TxtBox_AddCusAddress.TabIndex = 4;
            // 
            // TxtBox_AddCusName
            // 
            this.TxtBox_AddCusName.Location = new System.Drawing.Point(73, 19);
            this.TxtBox_AddCusName.Name = "TxtBox_AddCusName";
            this.TxtBox_AddCusName.Size = new System.Drawing.Size(221, 20);
            this.TxtBox_AddCusName.TabIndex = 3;
            // 
            // Btn_Submit
            // 
            this.Btn_Submit.Location = new System.Drawing.Point(22, 97);
            this.Btn_Submit.Name = "Btn_Submit";
            this.Btn_Submit.Size = new System.Drawing.Size(272, 32);
            this.Btn_Submit.TabIndex = 9;
            this.Btn_Submit.Text = "Submit";
            this.Btn_Submit.UseVisualStyleBackColor = true;
            this.Btn_Submit.Click += new System.EventHandler(this.Btn_Submit_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Phone Number:";
            // 
            // TxtBox_AddCusPhone
            // 
            this.TxtBox_AddCusPhone.Location = new System.Drawing.Point(106, 71);
            this.TxtBox_AddCusPhone.Name = "TxtBox_AddCusPhone";
            this.TxtBox_AddCusPhone.Size = new System.Drawing.Size(188, 20);
            this.TxtBox_AddCusPhone.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Address:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // CustomerInformationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 159);
            this.Controls.Add(this.groupBox3);
            this.Name = "CustomerInformationForm";
            this.Text = "Add Customer Information";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TxtBox_AddCusAddress;
        private System.Windows.Forms.TextBox TxtBox_AddCusName;
        private System.Windows.Forms.Button Btn_Submit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtBox_AddCusPhone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}