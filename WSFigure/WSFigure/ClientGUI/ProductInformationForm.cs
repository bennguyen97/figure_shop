﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WSFigure
{
    public partial class ProductInformationForm : Form
    {
        private String name = "";
        public ProductInformationForm()
        {
            InitializeComponent();
        }

        public ProductInformationForm(String name)
        {
            this.name = name;
            InitializeComponent();
        }

        private void ProductInformationForm_Load(object sender, EventArgs e)
        {
            Get_Search();
        }
        private void Btn_Return_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Get_Search()
        {
            String info = name;
            String colName = "Name";
            string url = "http://www.figuresoap.somee.com/api/FigureService/SearchFigure?info=" + info +
                "&colName=" + colName;
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Figure> figure = JsonConvert.DeserializeObject<List<Figure>>(json);
            foreach (var item in figure)
            {
                TxtBox_Name.Text = item.Name;
                TxtBox_Scale.Text = item.Scale;
                TxtBox_Manufacturer.Text = item.Manufacturer;
                PBox_Image.ImageLocation = item.Image;
                RTxtBox_Info.Text = item.Info;
            }
        }
    }
}
