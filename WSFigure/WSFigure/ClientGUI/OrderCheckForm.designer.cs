﻿namespace WSFigure
{
    partial class OrderCheckForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Dgv_ProductDetailed = new System.Windows.Forms.DataGridView();
            this.TxtBox_Total = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_editQuantity = new System.Windows.Forms.Button();
            this.Btn_Delete = new System.Windows.Forms.Button();
            this.Btn_Confirm = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_ProductDetailed)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Dgv_ProductDetailed);
            this.groupBox2.Controls.Add(this.TxtBox_Total);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.Btn_editQuantity);
            this.groupBox2.Controls.Add(this.Btn_Delete);
            this.groupBox2.Controls.Add(this.Btn_Confirm);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(563, 210);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Order List";
            // 
            // Dgv_ProductDetailed
            // 
            this.Dgv_ProductDetailed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv_ProductDetailed.Location = new System.Drawing.Point(16, 19);
            this.Dgv_ProductDetailed.Name = "Dgv_ProductDetailed";
            this.Dgv_ProductDetailed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv_ProductDetailed.Size = new System.Drawing.Size(363, 139);
            this.Dgv_ProductDetailed.TabIndex = 7;
            // 
            // TxtBox_Total
            // 
            this.TxtBox_Total.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBox_Total.Location = new System.Drawing.Point(385, 53);
            this.TxtBox_Total.Name = "TxtBox_Total";
            this.TxtBox_Total.ReadOnly = true;
            this.TxtBox_Total.Size = new System.Drawing.Size(172, 32);
            this.TxtBox_Total.TabIndex = 6;
            this.TxtBox_Total.Text = "0";
            this.TxtBox_Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(429, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 31);
            this.label1.TabIndex = 5;
            this.label1.Text = "Total:";
            // 
            // Btn_editQuantity
            // 
            this.Btn_editQuantity.Location = new System.Drawing.Point(112, 176);
            this.Btn_editQuantity.Name = "Btn_editQuantity";
            this.Btn_editQuantity.Size = new System.Drawing.Size(75, 23);
            this.Btn_editQuantity.TabIndex = 4;
            this.Btn_editQuantity.Text = "Edit Quantity";
            this.Btn_editQuantity.UseVisualStyleBackColor = true;
            this.Btn_editQuantity.Click += new System.EventHandler(this.Btn_editQuantity_Click);
            // 
            // Btn_Delete
            // 
            this.Btn_Delete.Location = new System.Drawing.Point(210, 176);
            this.Btn_Delete.Name = "Btn_Delete";
            this.Btn_Delete.Size = new System.Drawing.Size(75, 23);
            this.Btn_Delete.TabIndex = 3;
            this.Btn_Delete.Text = "Delete";
            this.Btn_Delete.UseVisualStyleBackColor = true;
            this.Btn_Delete.Click += new System.EventHandler(this.Btn_Delete_Click);
            // 
            // Btn_Confirm
            // 
            this.Btn_Confirm.Location = new System.Drawing.Point(304, 176);
            this.Btn_Confirm.Name = "Btn_Confirm";
            this.Btn_Confirm.Size = new System.Drawing.Size(75, 23);
            this.Btn_Confirm.TabIndex = 1;
            this.Btn_Confirm.Text = "Confirm";
            this.Btn_Confirm.UseVisualStyleBackColor = true;
            this.Btn_Confirm.Click += new System.EventHandler(this.Btn_Confirm_Click);
            // 
            // OrderCheckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 229);
            this.Controls.Add(this.groupBox2);
            this.Name = "OrderCheckForm";
            this.Text = "Cart View";
            this.Load += new System.EventHandler(this.OrderCheckForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_ProductDetailed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Btn_Confirm;
        private System.Windows.Forms.Button Btn_editQuantity;
        private System.Windows.Forms.Button Btn_Delete;
        private System.Windows.Forms.TextBox TxtBox_Total;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView Dgv_ProductDetailed;
    }
}