﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WSFigure
{
    public partial class QuantityForm : Form
    {
        private int Max_Quantity_Product = 0;

        public QuantityForm()
        {
            InitializeComponent();
        }

        public QuantityForm(int Max_Quantity_Product)
        {
            this.Max_Quantity_Product = Max_Quantity_Product;
            InitializeComponent();
        }

        public int Quantity
        {
            get { return int.Parse(Nup_Quantity.Value.ToString()); }
        }

        private void QuantityForm_Load(object sender, EventArgs e)
        {
            Nup_Quantity.Maximum = Max_Quantity_Product;
            L_MaxValue.Text = "(Stock: " + Max_Quantity_Product + ")";
        }

        private void Btn_Submit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
