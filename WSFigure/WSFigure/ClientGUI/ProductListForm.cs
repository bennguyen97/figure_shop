﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using Newtonsoft.Json;

namespace WSFigure
{
    public partial class ProductListForm : Form
    {
        bool Id_Session = false;
        int Id_Order = 0;
        int Total_Price = 0;

        public ProductListForm()
        {
            InitializeComponent();
        }

        private void ProductListForm_Load(object sender, EventArgs e)
        {
            Get_All();
            CbBox_Search.SelectedIndex = 0;
            DataGridViewRow dr = Dgv_FigureList.SelectedRows[0];
            Pb_Image.ImageLocation = dr.Cells["Image"].Value.ToString();
            Nup_Quantity.Maximum = Convert.ToDecimal(dr.Cells["Quantity"].Value.ToString());
            Nup_Quantity.Value = 1;
        }

        private void Dgv_FigureList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dr = Dgv_FigureList.SelectedRows[0];
            Pb_Image.ImageLocation = dr.Cells["Image"].Value.ToString();
            Nup_Quantity.Maximum = Convert.ToDecimal(dr.Cells["Quantity"].Value.ToString());
            Nup_Quantity.Value = 1;
        }
        
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            Search_Figure();
        }

        private void CbBox_Search_SelectedIndexChanged(object sender, EventArgs e)
        {
            Search_Figure();
        }

        private void Btn_AddtoCart_Click(object sender, EventArgs e)
        {
            if (Nup_Quantity.Value == 0)
            {
                MessageBox.Show("Quantity must be at least 1 to be able to add to cart!", "Notification");
            }
            else
            {
                if (Id_Session == false)
                {
                    Id_Session = true;
                    Orders ord = new Orders()
                    {
                        Id = 0,
                        OrderDate = DateTime.Now,
                        State = "",
                        Total = 0,
                        CustomerId = 0
                    };
                    String data = JsonConvert.SerializeObject(ord);
                    string url = "http://www.figuresoap.somee.com/api/FigureService/AddOrder";
                    WebClient client = new WebClient();
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    Id_Order = int.Parse(client.UploadString(new Uri(url), data));

                    Add_Order();
                    int quantity = int.Parse(Nup_Quantity.Value.ToString());
                    int price = int.Parse(Dgv_FigureList.CurrentRow.Cells[3].Value.ToString());
                    Total_Price += quantity * price;
                    
                   
                }
                else
                {
                    Add_Order();
                    int quantity = int.Parse(Nup_Quantity.Value.ToString());
                    int price = int.Parse(Dgv_FigureList.CurrentRow.Cells[3].Value.ToString());
                    Total_Price += quantity * price;
                    
                }
            }
        }

        private void Get_All()
        {
            string url = "http://www.figuresoap.somee.com/api/FigureService/GetAllFigure";
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Figure> figure = JsonConvert.DeserializeObject<List<Figure>>(json);
            LoadFigureFromList(figure);
        }

        private void LoadFigureFromList(List<Figure> figs)
        {
            Dgv_FigureList.BeginInvoke(new MethodInvoker(delegate
            {
                Dgv_FigureList.DataSource = figs;
                Dgv_FigureList.Refresh();

                Dgv_FigureList.Columns["Code"].HeaderText = "Product Code";
                Dgv_FigureList.Columns["Code"].ReadOnly = true;
                Dgv_FigureList.Columns["Code"].Width = 50;

                Dgv_FigureList.Columns["Name"].HeaderText = "Product Name";
                Dgv_FigureList.Columns["Name"].ReadOnly = true;
                Dgv_FigureList.Columns["Name"].Width = 200;

                Dgv_FigureList.Columns["Scale"].HeaderText = "Scale";
                Dgv_FigureList.Columns["Scale"].ReadOnly = true;
                Dgv_FigureList.Columns["Scale"].Width = 150;

                Dgv_FigureList.Columns["Price"].HeaderText = "Price";
                Dgv_FigureList.Columns["Price"].ReadOnly = true;
                Dgv_FigureList.Columns["Price"].Width = 70;

                Dgv_FigureList.Columns["Info"].HeaderText = "Info";
                Dgv_FigureList.Columns["Info"].ReadOnly = true;
                Dgv_FigureList.Columns["Info"].Width = 150;

                Dgv_FigureList.Columns["Quantity"].HeaderText = "Quantity";
                Dgv_FigureList.Columns["Quantity"].ReadOnly = true;
                Dgv_FigureList.Columns["Quantity"].Width = 50;

                Dgv_FigureList.Columns["Image"].HeaderText = "Image";
                Dgv_FigureList.Columns["Image"].ReadOnly = true;
                Dgv_FigureList.Columns["Image"].Width = 150;

                Dgv_FigureList.Columns["Manufacturer"].HeaderText = "Manufacturer";
                Dgv_FigureList.Columns["Manufacturer"].ReadOnly = true;
                Dgv_FigureList.Columns["Manufacturer"].Width = 150;

                Dgv_FigureList.Columns["Scale"].Visible = false;
                Dgv_FigureList.Columns["Info"].Visible = false;
                Dgv_FigureList.Columns["Image"].Visible = false;
                Dgv_FigureList.Columns["Manufacturer"].Visible = false;
            }));
        }

        private void Search_Figure()
        {
            String info = TxtBox_Search.Text;
            String colName = CbBox_Search.Text;
            string url = "http://www.figuresoap.somee.com/api/FigureService/SearchFigure?info=" + info +
                "&colName=" + colName;
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Figure> figure = JsonConvert.DeserializeObject<List<Figure>>(json);
            Dgv_FigureList.DataSource = figure;
        }

        private void Add_Order()
        {
            if (Id_Order > 0)
            {
                MessageBox.Show("New Item has been added to Cart!", "Notification");
                DataGridViewRow dr = Dgv_FigureList.SelectedRows[0];
                int productid = int.Parse(dr.Cells["Code"].Value.ToString());
                int price = int.Parse(dr.Cells["Price"].Value.ToString());
                int quantity = int.Parse(Nup_Quantity.Value.ToString());
                OrderDetailed ord_det = new OrderDetailed()
                {
                    OrderId = Id_Order,
                    ProductId = productid,
                    Quantity = quantity,
                    Price = price,
                    TotalPrice = quantity * price
                };
                String data_detailed = JsonConvert.SerializeObject(ord_det);
                string url_detailed = "http://www.figuresoap.somee.com/api/FigureService/InsertDetailedOrder";
                WebClient client_order = new WebClient();
                client_order.Headers[HttpRequestHeader.ContentType] = "application/json";
                bool result_detailed = bool.Parse(client_order.UploadString(new Uri(url_detailed), data_detailed));
            }
            else
            {
                MessageBox.Show("Failed to add new Item! Please try again!", "Notification");
            }
        }

        private void Btn_ViewCart_Click(object sender, EventArgs e)
        {
            OrderCheckForm ord_form = new OrderCheckForm(Id_Order, Total_Price);
            ord_form.ShowDialog();
        }

        private void Btn_Details_Click(object sender, EventArgs e)
        {
            DataGridViewRow dr = Dgv_FigureList.SelectedRows[0];
            String name = dr.Cells["Name"].Value.ToString();
            ProductInformationForm pro_info_form = new ProductInformationForm(name);
            pro_info_form.ShowDialog();
        }
    }
}
