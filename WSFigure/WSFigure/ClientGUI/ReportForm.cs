﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;

using WSFigure.BLL;

namespace WSFigure
{
    public partial class ReportForm : Form
    {
        private int Id_Order = 0;
        private List<Report> report = new List<Report>();
        public ReportForm()
        {
            InitializeComponent();
        }

        public ReportForm(int Id_Order)
        {
            this.Id_Order = Id_Order;
            InitializeComponent();
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            List<Report> report = new ReportBUS().GetOrderData(Id_Order);
            Dgv_Report.DataSource = report;
        }

        private void Btn_ExcelReport_Click(object sender, EventArgs e)
        {
            try
            {
                report = new ReportBUS().GetOrderData(Id_Order);
                String saveExcelFile = @"C:\Users\Ryodo\Desktop\Homework\Project\WSFigure\Report\excel_report_order_no_" + Id_Order + ".xlsx";

                //Start Excel
                Excel.Application xlApp = new Excel.Application();
                if (xlApp == null)
                {
                    MessageBox.Show("Error! Cannot use Excel Library");
                    return;
                }
                xlApp.Visible = false;

                object misValue = System.Reflection.Missing.Value;

                //Start Workbook
                Workbook wb = xlApp.Workbooks.Add(misValue);

                //Start Worksheet
                Worksheet ws = (Worksheet)wb.Worksheets[1];
                if (ws == null)
                {
                    MessageBox.Show("Không thể tạo được WorkSheet");
                    return;
                }

                int row = 1;
                string fontName1 = "Showcard Gothic";
                string fontName2 = "Clarendon Blk BT";
                string fontName3 = "Times New Roman";
                int fontSizeTitle = 24;
                int fontSizeHeader = 14;
                int fontSizeContent = 12;

                Range row_Title = ws.get_Range("A1", "C1");
                row_Title.Merge();
                row_Title.Font.Size = fontSizeTitle;
                row_Title.Font.Name = fontName1;
                row_Title.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                row_Title.Value2 = "Order Receipt";
                row_Title.Interior.Color = ColorTranslator.ToOle(System.Drawing.Color.LightSalmon); //Style
                row_Title.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.Red);
                row_Title.Font.Bold = true;

                Range row_Date= ws.get_Range("A2", "B2");
                row_Date.Merge();
                row_Date.Font.Size = fontSizeContent;
                row_Date.Font.Name = fontName2;
                row_Date.Value2 = "Date: " + report[0].OrderDate;
                row_Date.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.ForestGreen);

                Range row_Id = ws.get_Range("A3", "B3");
                row_Id.Merge();
                row_Id.Font.Size = fontSizeContent;
                row_Id.Font.Name = fontName2;
                row_Id.Value2 = "Order No." + report[0].Id;
                row_Id.Font.Bold = true;

                Range row_Customer = ws.get_Range("C2", "C2");
                row_Customer.Font.Size = fontSizeContent;
                row_Customer.Font.Name = fontName2;
                row_Customer.Value2 = "Customer: " + report[0].CustomerName;
                row_Customer.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.BlueViolet);

                Range row_State = ws.get_Range("C3", "C3");
                row_State.Font.Size = fontSizeContent;
                row_State.Font.Name = fontName2;
                row_State.Value2 = "Status: " + report[0].State;
                row_State.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.DarkMagenta);

                Range row_FigName = ws.get_Range("A4", "A5");
                row_FigName.Merge();
                row_FigName.Font.Size = fontSizeHeader;
                row_FigName.Font.Name = fontName2;
                row_FigName.Cells.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                row_FigName.Value2 = "Figure Name";
                row_FigName.ColumnWidth = 100;
                row_FigName.Interior.Color = ColorTranslator.ToOle(System.Drawing.Color.LightSkyBlue); //Style
                row_FigName.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.DarkBlue);
                row_FigName.Font.Bold = true;

                Range row_Quantity = ws.get_Range("B4", "B5");
                row_Quantity.Merge();
                row_Quantity.Font.Size = fontSizeHeader;
                row_Quantity.Font.Name = fontName2;
                row_Quantity.Cells.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                row_Quantity.Value2 = "Quantity";
                row_Quantity.ColumnWidth = 20;
                row_Quantity.Interior.Color = ColorTranslator.ToOle(System.Drawing.Color.LightSkyBlue); //Style
                row_Quantity.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.DarkBlue);
                row_Quantity.Font.Bold = true;

                Range row_Price = ws.get_Range("C4", "C5");
                row_Price.Merge();
                row_Price.Font.Size = fontSizeHeader;
                row_Price.Font.Name = fontName2;
                row_Price.Cells.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                row_Price.Value2 = "Price";
                row_Price.ColumnWidth = 40;
                row_Price.Interior.Color = ColorTranslator.ToOle(System.Drawing.Color.LightSkyBlue); //Style
                row_Price.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.DarkBlue);
                row_Price.Font.Bold = true;


                int no = 0;
                row = 5;

                foreach (Report rp in report)
                {
                    no++;
                    row++;
                    dynamic[] arr = { rp.FigureName, rp.Quantity, rp.Price };
                    Range rowData = ws.get_Range("A" + row, "C" + row);
                    rowData.Font.Size = fontSizeContent;
                    rowData.Font.Name = fontName3;
                    rowData.Cells.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    rowData.Value2 = arr;
                }

                Range row_Total_text = ws.get_Range("A" + (row + 1), "B" + (row + 1));
                row_Total_text.Merge();
                row_Total_text.Font.Size = fontSizeContent;
                row_Total_text.Font.Name = fontName2;
                row_Total_text.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                row_Total_text.Value2 = "Total";
                row_Total_text.Interior.Color = ColorTranslator.ToOle(System.Drawing.Color.Crimson); //Style
                row_Total_text.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.LightSalmon);
                row_Total_text.Font.Bold = true;

                Range row_Total = ws.get_Range("C" + (row + 1), "C" + (row + 1));
                row_Total.Font.Size = fontSizeContent;
                row_Total.Font.Name = fontName2;
                row_Total.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                row_Total.Value2 = report[0].Total;
                row_Total.Font.Color = ColorTranslator.ToOle(System.Drawing.Color.Red);

                BorderAround(ws.get_Range("A1", "C" + (row + 1)));

                //Save Excel File in Disk
                wb.SaveAs(saveExcelFile);

                //Close Excel File to complete Saving
                wb.Close(true, misValue, misValue);

                //Exit Application and regain memory for COM
                xlApp.Quit();
                releaseObject(ws);
                releaseObject(wb);
                releaseObject(xlApp);

                //Open Excel File after Saving
                System.Diagnostics.Process.Start(saveExcelFile);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void BorderAround(Range range)
        {
            Borders borders = range.Borders;
            borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
            borders.Color = Color.Black;
            borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlDiagonalUp].LineStyle = XlLineStyle.xlLineStyleNone;
            borders[XlBordersIndex.xlDiagonalDown].LineStyle = XlLineStyle.xlLineStyleNone;
        }

        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                obj = null;
            }
            finally
            { GC.Collect(); }
        }
    }
}
