﻿namespace WSFigure
{
    partial class ReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportForm));
            this.Dgv_Report = new System.Windows.Forms.DataGridView();
            this.Btn_ExcelReport = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_Report)).BeginInit();
            this.SuspendLayout();
            // 
            // Dgv_Report
            // 
            this.Dgv_Report.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv_Report.Location = new System.Drawing.Point(17, 72);
            this.Dgv_Report.Name = "Dgv_Report";
            this.Dgv_Report.Size = new System.Drawing.Size(844, 253);
            this.Dgv_Report.TabIndex = 0;
            // 
            // Btn_ExcelReport
            // 
            this.Btn_ExcelReport.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_ExcelReport.BackgroundImage")));
            this.Btn_ExcelReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_ExcelReport.Location = new System.Drawing.Point(318, 331);
            this.Btn_ExcelReport.Name = "Btn_ExcelReport";
            this.Btn_ExcelReport.Size = new System.Drawing.Size(278, 82);
            this.Btn_ExcelReport.TabIndex = 9;
            this.Btn_ExcelReport.UseVisualStyleBackColor = true;
            this.Btn_ExcelReport.Click += new System.EventHandler(this.Btn_ExcelReport_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(205, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(496, 60);
            this.label1.TabIndex = 10;
            this.label1.Text = "- Receipt Report -";
            // 
            // ReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 431);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Btn_ExcelReport);
            this.Controls.Add(this.Dgv_Report);
            this.Name = "ReportForm";
            this.Text = "Receipt Report";
            this.Load += new System.EventHandler(this.ReportForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_Report)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Dgv_Report;
        private System.Windows.Forms.Button Btn_ExcelReport;
        private System.Windows.Forms.Label label1;
    }
}