﻿namespace WSFigure
{
    partial class ProductListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Search = new System.Windows.Forms.GroupBox();
            this.Btn_Search = new System.Windows.Forms.Button();
            this.CbBox_Search = new System.Windows.Forms.ComboBox();
            this.TxtBox_Search = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Btn_ViewCart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Nup_Quantity = new System.Windows.Forms.NumericUpDown();
            this.Dgv_FigureList = new System.Windows.Forms.DataGridView();
            this.Btn_AddtoCart = new System.Windows.Forms.Button();
            this.Pb_Image = new System.Windows.Forms.PictureBox();
            this.Btn_Details = new System.Windows.Forms.Button();
            this.Search.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Nup_Quantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_FigureList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pb_Image)).BeginInit();
            this.SuspendLayout();
            // 
            // Search
            // 
            this.Search.Controls.Add(this.Btn_Search);
            this.Search.Controls.Add(this.CbBox_Search);
            this.Search.Controls.Add(this.TxtBox_Search);
            this.Search.Location = new System.Drawing.Point(12, 12);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(460, 53);
            this.Search.TabIndex = 3;
            this.Search.TabStop = false;
            this.Search.Text = "Search";
            // 
            // Btn_Search
            // 
            this.Btn_Search.Location = new System.Drawing.Point(379, 19);
            this.Btn_Search.Name = "Btn_Search";
            this.Btn_Search.Size = new System.Drawing.Size(75, 21);
            this.Btn_Search.TabIndex = 7;
            this.Btn_Search.Text = "Search";
            this.Btn_Search.UseVisualStyleBackColor = true;
            this.Btn_Search.Click += new System.EventHandler(this.Btn_Search_Click);
            // 
            // CbBox_Search
            // 
            this.CbBox_Search.FormattingEnabled = true;
            this.CbBox_Search.Items.AddRange(new object[] {
            "Name",
            "Scale",
            "Info",
            "Manufacturer"});
            this.CbBox_Search.Location = new System.Drawing.Point(246, 19);
            this.CbBox_Search.Name = "CbBox_Search";
            this.CbBox_Search.Size = new System.Drawing.Size(127, 21);
            this.CbBox_Search.TabIndex = 6;
            this.CbBox_Search.SelectedIndexChanged += new System.EventHandler(this.CbBox_Search_SelectedIndexChanged);
            // 
            // TxtBox_Search
            // 
            this.TxtBox_Search.Location = new System.Drawing.Point(16, 19);
            this.TxtBox_Search.Name = "TxtBox_Search";
            this.TxtBox_Search.Size = new System.Drawing.Size(224, 20);
            this.TxtBox_Search.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Btn_ViewCart);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.Nup_Quantity);
            this.groupBox2.Controls.Add(this.Dgv_FigureList);
            this.groupBox2.Controls.Add(this.Btn_AddtoCart);
            this.groupBox2.Location = new System.Drawing.Point(12, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(460, 212);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Figure List";
            // 
            // Btn_ViewCart
            // 
            this.Btn_ViewCart.Location = new System.Drawing.Point(16, 175);
            this.Btn_ViewCart.Name = "Btn_ViewCart";
            this.Btn_ViewCart.Size = new System.Drawing.Size(88, 22);
            this.Btn_ViewCart.TabIndex = 12;
            this.Btn_ViewCart.Text = "View Cart";
            this.Btn_ViewCart.UseVisualStyleBackColor = true;
            this.Btn_ViewCart.Click += new System.EventHandler(this.Btn_ViewCart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 180);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Quantity:";
            // 
            // Nup_Quantity
            // 
            this.Nup_Quantity.Location = new System.Drawing.Point(232, 177);
            this.Nup_Quantity.Name = "Nup_Quantity";
            this.Nup_Quantity.Size = new System.Drawing.Size(120, 20);
            this.Nup_Quantity.TabIndex = 10;
            // 
            // Dgv_FigureList
            // 
            this.Dgv_FigureList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv_FigureList.Location = new System.Drawing.Point(16, 19);
            this.Dgv_FigureList.MultiSelect = false;
            this.Dgv_FigureList.Name = "Dgv_FigureList";
            this.Dgv_FigureList.ReadOnly = true;
            this.Dgv_FigureList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv_FigureList.Size = new System.Drawing.Size(430, 150);
            this.Dgv_FigureList.TabIndex = 0;
            this.Dgv_FigureList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_FigureList_CellClick);
            // 
            // Btn_AddtoCart
            // 
            this.Btn_AddtoCart.Location = new System.Drawing.Point(358, 175);
            this.Btn_AddtoCart.Name = "Btn_AddtoCart";
            this.Btn_AddtoCart.Size = new System.Drawing.Size(88, 22);
            this.Btn_AddtoCart.TabIndex = 8;
            this.Btn_AddtoCart.Text = "Add to Cart...";
            this.Btn_AddtoCart.UseVisualStyleBackColor = true;
            this.Btn_AddtoCart.Click += new System.EventHandler(this.Btn_AddtoCart_Click);
            // 
            // Pb_Image
            // 
            this.Pb_Image.Location = new System.Drawing.Point(478, 12);
            this.Pb_Image.Name = "Pb_Image";
            this.Pb_Image.Size = new System.Drawing.Size(400, 271);
            this.Pb_Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Pb_Image.TabIndex = 5;
            this.Pb_Image.TabStop = false;
            // 
            // Btn_Details
            // 
            this.Btn_Details.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Details.ForeColor = System.Drawing.Color.Red;
            this.Btn_Details.Location = new System.Drawing.Point(772, 246);
            this.Btn_Details.Name = "Btn_Details";
            this.Btn_Details.Size = new System.Drawing.Size(106, 37);
            this.Btn_Details.TabIndex = 6;
            this.Btn_Details.Text = "DETAILS";
            this.Btn_Details.UseVisualStyleBackColor = true;
            this.Btn_Details.Click += new System.EventHandler(this.Btn_Details_Click);
            // 
            // ProductListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 297);
            this.Controls.Add(this.Btn_Details);
            this.Controls.Add(this.Pb_Image);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Search);
            this.Name = "ProductListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Figure List";
            this.Load += new System.EventHandler(this.ProductListForm_Load);
            this.Search.ResumeLayout(false);
            this.Search.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Nup_Quantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_FigureList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pb_Image)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Search;
        private System.Windows.Forms.ComboBox CbBox_Search;
        private System.Windows.Forms.TextBox TxtBox_Search;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Btn_ViewCart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown Nup_Quantity;
        private System.Windows.Forms.DataGridView Dgv_FigureList;
        private System.Windows.Forms.Button Btn_AddtoCart;
        private System.Windows.Forms.Button Btn_Search;
        private System.Windows.Forms.PictureBox Pb_Image;
        private System.Windows.Forms.Button Btn_Details;
    }
}