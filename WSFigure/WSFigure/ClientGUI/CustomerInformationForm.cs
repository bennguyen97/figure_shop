﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WSFigure.BLL;
namespace WSFigure
{
    public partial class CustomerInformationForm : Form
    {
        private int Id_Order = 0;
        private int Total_Price = 0;
        public CustomerInformationForm()
        {
            InitializeComponent();
        }

        public CustomerInformationForm(int Id_Order, int Total_Price)
        {
            this.Id_Order = Id_Order;
            this.Total_Price = Total_Price;
            InitializeComponent();
        }

        private void Btn_Submit_Click_1(object sender, EventArgs e)
        {
            if (TxtBox_AddCusName.Text != "" && TxtBox_AddCusAddress.Text != "" && TxtBox_AddCusPhone.Text != "")
            {
                List<Customer> customer = Search_Customer();
                if (customer.Any())
                {
                    string message = "Your phone number has already exist. Do you want to use this number?";
                    string title = "Confirm";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult log_result = MessageBox.Show(message, title, buttons);
                    if (log_result == DialogResult.Yes)
                    {
                        Orders o = new Orders()
                        {
                            Id = Id_Order,
                            OrderDate = DateTime.Now,
                            State = "pending",
                            Total = Total_Price,
                            CustomerId = customer[0].Id
                        };
                        bool result = new OrderBUS().UpdateOrder(o);
                        if (result)
                        {
                            MessageBox.Show("Order Successfully!", "Notification");
                            ReportForm rep_form = new ReportForm(Id_Order);
                            rep_form.ShowDialog();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Order Failed! Please try again!", "Notification");
                        }
                    }
                    else
                    {
                        //Do nothing
                    }
                }
                else
                {
                    Customer cus = new Customer()
                    {
                        Id = 0,
                        Name = TxtBox_AddCusName.Text,
                        Address = TxtBox_AddCusAddress.Text,
                        PhoneNumber = TxtBox_AddCusPhone.Text
                    };
                    int cusId = new CustomerBUS().InsertCustomer(cus);
                    if (cusId > 0)
                    {
                        Orders o = new Orders()
                        {
                            Id = Id_Order,
                            OrderDate = DateTime.Now,
                            State = "pending",
                            Total = Total_Price,
                            CustomerId = cusId
                        };
                        bool result = new OrderBUS().UpdateOrder(o);
                        if (result)
                        {
                            MessageBox.Show("Order Successfully!", "Notification");
                            ReportForm rep_form = new ReportForm(Id_Order);
                            rep_form.ShowDialog();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Order Failed! Please try again!", "Notification");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("You have to fill in all of these fields to continue!", "Notification");
            }
        }

    private List<Customer> Search_Customer()
        {
            String info = TxtBox_AddCusPhone.Text;
            String colName = "phonenumber";
            string url = "http://www.figuresoap.somee.com/api/FigureService/SearchCustomer?info=" + info +
                "&colName=" + colName;
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Customer> customer = JsonConvert.DeserializeObject<List<Customer>>(json);
            return customer;
        }
    }
}
