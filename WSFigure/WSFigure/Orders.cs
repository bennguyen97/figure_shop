﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSFigure
{
    class Orders
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public string State { get; set; }
        public int Total { get; set; }
        public int CustomerId { get; set; }
    }
}
