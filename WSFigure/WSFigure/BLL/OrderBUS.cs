﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WSFigure.DAL;

namespace WSFigure.BLL
{
    class OrderBUS
    {
        public bool UpdateOrder(Orders o)
        {
            bool result = new OrderDAO().UpdateOrder(o);
            return result;
        }
    }
}
