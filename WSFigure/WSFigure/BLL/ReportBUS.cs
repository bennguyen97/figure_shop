﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WSFigure.DAL;

namespace WSFigure.BLL
{
    class ReportBUS
    {
        public List<Report> GetOrderData(int ordersId)
        {
            List<Report> reports = new ReportDAO().GetOrderData(ordersId);
            return reports;
        }
    }
}
