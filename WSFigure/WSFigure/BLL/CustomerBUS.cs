﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WSFigure.DAL;

namespace WSFigure.BLL
{
    class CustomerBUS
    {
        public int InsertCustomer(Customer cus)
        {
            int result = new CustomerDAO().InsertCustomer(cus);
            return result;
        }
    }
}
