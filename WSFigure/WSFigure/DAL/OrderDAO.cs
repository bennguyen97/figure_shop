﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSFigure.DAL
{
    class OrderDAO
    {
        public bool UpdateOrder(Orders orders)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "UPDATE ORDERS SET order_date = @order_date, state = @state, total = @total, customer_id = @customer_id WHERE id = @id";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@id", orders.Id));
            com.Parameters.Add(new SqlParameter("@order_date", orders.OrderDate));
            com.Parameters.Add(new SqlParameter("@state", orders.State));
            com.Parameters.Add(new SqlParameter("@total", orders.Total));
            com.Parameters.Add(new SqlParameter("@customer_id", orders.CustomerId));
            return com.ExecuteNonQuery() > 0;
        }
    }
}
