﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSFigure.DAL
{
    class ReportDAO
    {
        public List<Report> GetOrderData(int ordersId)
        {
            List<Report> reportData = new List<Report>();
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT o.id, o.order_date, o.state, c.name as customer_name, f.name, od.quantity, od.price, o.total " +
                "FROM Figure f, Orders o, Customer c, Order_Detailed od " +
                "WHERE f.code = od.product_id AND o.id = od.order_id AND o.customer_id = c.id AND o.id = @id " +
                "ORDER BY f.code ASC";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@id", ordersId));
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Report report = new Report()
                {
                    Id = (int)dr["id"],
                    OrderDate = (DateTime)dr["order_date"],
                    State = (String)dr["state"],
                    CustomerName = (String)dr["customer_name"],
                    FigureName = (String)dr["name"],
                    Quantity = (int)dr["quantity"],
                    Price = (int)dr["price"],
                    Total = (int)dr["total"]
                };
                reportData.Add(report);
            }
            return reportData;
        }
    }
}
