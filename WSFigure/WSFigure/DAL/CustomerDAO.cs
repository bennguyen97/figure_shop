﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSFigure.DAL
{
    class CustomerDAO
    {
        public int InsertCustomer(Customer customer)
        {
            int cusId = 0;
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "INSERT INTO CUSTOMER OUTPUT INSERTED.id VALUES(@name, @address, @phonenumber)";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@name", customer.Name));
            com.Parameters.Add(new SqlParameter("@address", customer.Address));
            com.Parameters.Add(new SqlParameter("@phonenumber", customer.PhoneNumber));
            cusId = (int)com.ExecuteScalar();
            return cusId;
        }
    }
}
