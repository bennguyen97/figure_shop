﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSFigure
{
    class Report
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public string State { get; set; }
        public string CustomerName { get; set; }
        public string FigureName { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public int Total { get; set; }
    }
}
