USE [master]
GO
/****** Object:  Database [2152998]    Script Date: 6/27/2019 1:31:48 AM ******/
CREATE DATABASE [2152998]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'2152998_Data', FILENAME = N'c:\dzsqls\2152998.mdf' , SIZE = 8192KB , MAXSIZE = 15360KB , FILEGROWTH = 7168KB )
 LOG ON 
( NAME = N'2152998_Logs', FILENAME = N'c:\dzsqls\2152998.ldf' , SIZE = 8192KB , MAXSIZE = 20480KB , FILEGROWTH = 12288KB )
GO
ALTER DATABASE [2152998] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [2152998].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [2152998] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [2152998] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [2152998] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [2152998] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [2152998] SET ARITHABORT OFF 
GO
ALTER DATABASE [2152998] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [2152998] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [2152998] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [2152998] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [2152998] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [2152998] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [2152998] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [2152998] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [2152998] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [2152998] SET  ENABLE_BROKER 
GO
ALTER DATABASE [2152998] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [2152998] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [2152998] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [2152998] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [2152998] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [2152998] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [2152998] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [2152998] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [2152998] SET  MULTI_USER 
GO
ALTER DATABASE [2152998] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [2152998] SET DB_CHAINING OFF 
GO
ALTER DATABASE [2152998] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [2152998] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [2152998] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [2152998] SET QUERY_STORE = OFF
GO
USE [2152998]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [2152998]
GO
/****** Object:  User [Phuoc_97_SQLLogin_1]    Script Date: 6/27/2019 1:31:55 AM ******/
CREATE USER [Phuoc_97_SQLLogin_1] FOR LOGIN [Phuoc_97_SQLLogin_1] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [Phuoc_97_SQLLogin_1]
GO
/****** Object:  Schema [Phuoc_97_SQLLogin_1]    Script Date: 6/27/2019 1:31:57 AM ******/
CREATE SCHEMA [Phuoc_97_SQLLogin_1]
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 6/27/2019 1:31:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[address] [nvarchar](50) NOT NULL,
	[phonenumber] [nchar](10) NOT NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FIGURE]    Script Date: 6/27/2019 1:31:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FIGURE](
	[code] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[scale] [nvarchar](50) NOT NULL,
	[price] [int] NOT NULL,
	[info] [nvarchar](max) NULL,
	[quantity] [int] NOT NULL,
	[image] [nvarchar](60) NULL,
	[manufacturer] [nvarchar](50) NULL,
 CONSTRAINT [PK_FIGURE] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FigureList]    Script Date: 6/27/2019 1:31:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FigureList](
	[Code] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Scale] [nvarchar](50) NOT NULL,
	[Price] [int] NOT NULL,
	[Supplier] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FigureList] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ORDER_DETAILED]    Script Date: 6/27/2019 1:32:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ORDER_DETAILED](
	[order_id] [int] NOT NULL,
	[product_id] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[price] [int] NULL,
	[total_price] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ORDERS]    Script Date: 6/27/2019 1:32:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ORDERS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order_date] [date] NOT NULL,
	[state] [nvarchar](10) NOT NULL,
	[total] [int] NULL,
	[customer_id] [int] NULL,
 CONSTRAINT [PK_ORDERS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CUSTOMER] ON 

INSERT [dbo].[CUSTOMER] ([id], [name], [address], [phonenumber]) VALUES (1, N'Nguyen Phuoc', N'Nguyen Van Trang', N'0932023900')
INSERT [dbo].[CUSTOMER] ([id], [name], [address], [phonenumber]) VALUES (2, N'Long Le', N'Hoc Mon', N'0301121134')
INSERT [dbo].[CUSTOMER] ([id], [name], [address], [phonenumber]) VALUES (3, N'Chuong Lam', N'Quang Trung', N'0909113114')
SET IDENTITY_INSERT [dbo].[CUSTOMER] OFF
SET IDENTITY_INSERT [dbo].[FIGURE] ON 

INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (1, N'Hg RX-78-2 Gundam Revive', N'1/144', 250000, N'The RX-78-2 Gundam (Revive) is a 1/144 scale High Grade Universal Century (HGUC) kit released in 2015.

Includes: 
RX-78-2 Gundam
1 pair of Open hands 
trigger right hand
2 Beam Sabers
Beam Rifle
Shield
Hyper Bazooka
Joint part for Bazooka', 10, N'https://www.1999.co.jp/itbig32/10324628p.jpg', N'Bandai')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (2, N'Hg  RX-77-2 Guncannon (Revive Ver.)', N'1/144', 280000, N'The High Grade Universal Century (HGUC) RX-77-2 Guncannon (Revive Ver.) is a 1/144 scale kit released in 2015.

Includes:
Guncannon
Beam Rifle
1 pair Open hands', 5, N'https://www.1999.co.jp/itbig31/10319730p.jpg', N'Bandai')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (4, N'Hg GN-0000DVR Gundam 00 Diver', N'1/144', 270000, N'The High Grade Build Divers (HGBD) GN-0000DVR Gundam 00 Diver is a 1/144 scale kit released in 2018.

Includes:
Gundam 00 Diver
2 GN Beam Saber hilts
GN Sword II (x2)
1 pair of open hands
1 pair of normal hands (alternate)
1 pair of trigger hands', 9, N'https://www.1999.co.jp/itbig51/10517933p.jpg', N'Bandai')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (5, N'Hg GAT-X105B/FP Build Strike Gundam Full Package', N'1/144', 150000, N'The GAT-X105B/FP Build Strike Gundam Full Package is a 1/144 scale High Grade Build Fighters (HGBF) kit released in 2013.

Includes:
Build Strike Gundam
Enhanced Beam Rifle
Beam Rifle
Beam Gun
2 Beam Sabers
Shield
Build Booster', 20, N'https://www.1999.co.jp/itbig23/10235292p.jpg', N'Daban')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (7, N'Hg GAT-X131 Calamity Gundam', N'1/144', 120000, N'The 1/144 Gundam SEED Model Series (NG SEED) GAT-X131 Calamity Gundam is a 1/144 Scale kit released in 2003.

Includes:
Calamity Gundam
Plasma-Sabot Bazooka
Shield/Ram Cannon', 16, N'https://www.1999.co.jp/itbig16/10169409b3.jpg', N'Gao Gao (TT Hongli)')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (8, N'Hg PF-73-3BL Gundam Lightning Black Warrior', N'1/144', 380000, N'The PF-78-3A Gundam Amazing Red Warrior is a 1/144 scale High Grade Build Fighters (HGBF) kit released in 2017.

Includes
Gundam Lightning Black Warrior
Hyper Bazooka
Arm-mounted Beam Saber
Shield
Hand Gun
Gunblade (short)
Gunblade (long)
Grip
Beam Gatling
Gatling Gun nozzle
E-Pac
Beam Rifle Ko
Beam Rifle nozzle
Sensor
Beam Rifle Otsu
Neo Rifle', 3, N'https://www.1999.co.jp/itbig48/10480436p.jpg', N'Bandai')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (9, N'Mg GAT-X105+AQM/E-X01 Aile Strike Gundam', N'1/100', 1150000, N'The Gundam SEED Model Series (1/100 Gundam SEED) GAT-X105+AQM/E-X01 Aile Strike Gundam is a 1/100 Scale kit released in 2002.

Includes:
Strike Gundam
Beam Rifle
Shield
Aile Striker
2 Beam Sabers
Right trigger hand (Beam Rifle)
1/20 Kira Yamato figurine', 1, N'https://www.1999.co.jp/itbig22/10220105b.jpg', N'Bandai')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (10, N'Mg RX 93 HI NU', N'1/100', 800000, N'From the novel Mobile Suit Gundam: Char''s Counterattack - Beltorchika''s Children, Izubuchi Yutaka’s newly redesigned Hi-v Gundam rockets into the Master Grade lineup! 
- Display base with adjustable mounting angle is included.
- Hi-ν Gundam Base allows for display of the fin funnels in action! 
- Fin funnels expand into attack form. 
- Thrusters feature plated colors.
- Jet flame parts included to reproduce the flying fin funnels’ thrust! 
- Fin funnel beam blast also recreated using clear elements. 

- Accessories: Beam rifle x1, Bazooka x1, Beam Saber x3, Fin Funnel x6,
Shield x1.', 6, N'https://www.1999.co.jp/itbig05/10054193p.jpg', N'MC Model')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (12, N'HgCE Strike Freedom Gundam', N'1/144', 500000, N'- The first three-dimensional in the Strike Freedom Gundam HGCE
- HGCE series unique sophisticated and stylish design
- High Matt full burst mode can be reproduced by the range of motion improved
- In addition to the balanced proportions of the balance, when the wing is deployed, reproduce the beautiful shape having both storage.
- Railgun of lumbar reproduce the deployment gimmick. Of course, to the front, also movable to the waist back.

- Accessories: MA-M21KF high-energy beam rifle × 2, MA-M02G Gerhard Pale Rakeruta beam saber × 2, MX2200 beam shield', 18, N'https://www.1999.co.jp/itbig41/10413710p.jpg', N'Bandai')
INSERT [dbo].[FIGURE] ([code], [name], [scale], [price], [info], [quantity], [image], [manufacturer]) VALUES (13, N'HgCE Destiny Gundam ', N'1/144', 650000, N'Destiny Gundam is more advanced than HG''s latest format!
 - Completely new modeled joint structure allows you to reproduce action poses just like in the play.
- Dual joints built into the fuselage allow you to reproduce characteristic front bending action in Destiny Gundam.
- Side armor is constructed to avoid interference with the leg by sliding backward, not limiting the movable range.
- Also a symbol of Destiny Gundam [Wings of Light] comes with clear parts.
- Hand grips that can hold weapons and open hands for [Palma Fiocina] are included.
 The beam effect of [Palma Fiocina] is reproduced with clear parts.
- Aaronite, high energy long range beam cannon, beam boomerang, others, plentiful armed also attached!

Accessories: Arondody beam,sword × 1, High energy long range range beam gun × 1, Shield × 1, Beam shield × 1, High energy beam rifle × 1, Beam boomerang × 2, Wings of light × 1 formula, Palma Fiocina effect parts × 1, Hand for Parma Fio Keyina × Left and Right 1', 10, N'https://www.1999.co.jp/itbig58/10586232p.jpg', N'Bandai')
SET IDENTITY_INSERT [dbo].[FIGURE] OFF

INSERT [dbo].[ORDER_DETAILED] ([order_id], [product_id], [quantity], [price], [total_price]) VALUES (1, 4, 1, 270000, 270000)
INSERT [dbo].[ORDER_DETAILED] ([order_id], [product_id], [quantity], [price], [total_price]) VALUES (5, 5, 20, 150000, 3000000)
INSERT [dbo].[ORDER_DETAILED] ([order_id], [product_id], [quantity], [price], [total_price]) VALUES (64, 5, 3, 150000, 450000)
INSERT [dbo].[ORDER_DETAILED] ([order_id], [product_id], [quantity], [price], [total_price]) VALUES (64, 2, 2, 280000, 560000)
INSERT [dbo].[ORDER_DETAILED] ([order_id], [product_id], [quantity], [price], [total_price]) VALUES (64, 9, 1, 1150000, 1150000)
INSERT [dbo].[ORDER_DETAILED] ([order_id], [product_id], [quantity], [price], [total_price]) VALUES (64, 7, 2, 120000, 240000)
SET IDENTITY_INSERT [dbo].[ORDERS] ON 

INSERT [dbo].[ORDERS] ([id], [order_date], [state], [total], [customer_id]) VALUES (1, CAST(N'2019-06-01' AS Date), N'success', 270000, 1)
INSERT [dbo].[ORDERS] ([id], [order_date], [state], [total], [customer_id]) VALUES (5, CAST(N'2019-06-01' AS Date), N'cancel', 0, 3)
INSERT [dbo].[ORDERS] ([id], [order_date], [state], [total], [customer_id]) VALUES (23, CAST(N'2019-06-25' AS Date), N'cancel', 0, 0)
INSERT [dbo].[ORDERS] ([id], [order_date], [state], [total], [customer_id]) VALUES (24, CAST(N'2019-06-25' AS Date), N'cancel', 0, 0)
INSERT [dbo].[ORDERS] ([id], [order_date], [state], [total], [customer_id]) VALUES (25, CAST(N'2019-06-25' AS Date), N'cancel', 0, 0)
SET IDENTITY_INSERT [dbo].[ORDERS] OFF
ALTER TABLE [dbo].[ORDER_DETAILED]  WITH CHECK ADD  CONSTRAINT [FK_ORDER_DETAILED_FIGURE] FOREIGN KEY([product_id])
REFERENCES [dbo].[FIGURE] ([code])
GO
ALTER TABLE [dbo].[ORDER_DETAILED] CHECK CONSTRAINT [FK_ORDER_DETAILED_FIGURE]
GO
USE [master]
GO
ALTER DATABASE [2152998] SET  READ_WRITE 
GO
