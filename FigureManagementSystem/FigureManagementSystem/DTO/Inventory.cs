﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FigureManagementSystem.DTO
{
    class Inventory
    {
        public int Code { get; set; }
        public string Name { get; set; }
        public int Stock { get; set; }
        public int Sold { get; set; }
    }
}
