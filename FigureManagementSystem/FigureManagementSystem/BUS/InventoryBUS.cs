﻿using FigureManagementSystem.DAL;
using FigureManagementSystem.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FigureManagementSystem.BUS
{
    class InventoryBUS
    {
        public List<Inventory> GetAllInventory()
        {
            List<Inventory> Inventories = new InventoryDAO().SelectAllInventory();
            return Inventories;
        }

        public int CalculateIncome(int month, int date, int year)
        {
            int income = new InventoryDAO().TotalIncome(month, date, year);
            return income;
        }
    }
}
