﻿namespace FigureManagementSystem.GUI
{
    partial class CustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Search = new System.Windows.Forms.GroupBox();
            this.Btn_Search = new System.Windows.Forms.Button();
            this.CbBox_Search = new System.Windows.Forms.ComboBox();
            this.TxtBox_Search = new System.Windows.Forms.TextBox();
            this.CustomerList = new System.Windows.Forms.DataGridView();
            this.groupBox_Options = new System.Windows.Forms.GroupBox();
            this.Btn_Refresh = new System.Windows.Forms.Button();
            this.Btn_Update = new System.Windows.Forms.Button();
            this.groupBox_Details = new System.Windows.Forms.GroupBox();
            this.TxtBox_Id = new System.Windows.Forms.TextBox();
            this.label_Code = new System.Windows.Forms.Label();
            this.TxtBox_Address = new System.Windows.Forms.TextBox();
            this.label_Price = new System.Windows.Forms.Label();
            this.label_Name = new System.Windows.Forms.Label();
            this.TxtBox_PhoneNumber = new System.Windows.Forms.TextBox();
            this.TxtBox_Name = new System.Windows.Forms.TextBox();
            this.label_Producer = new System.Windows.Forms.Label();
            this.Customer = new System.Windows.Forms.GroupBox();
            this.Btn_Return = new System.Windows.Forms.Button();
            this.Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerList)).BeginInit();
            this.groupBox_Options.SuspendLayout();
            this.groupBox_Details.SuspendLayout();
            this.Customer.SuspendLayout();
            this.SuspendLayout();
            // 
            // Search
            // 
            this.Search.Controls.Add(this.Btn_Search);
            this.Search.Controls.Add(this.CbBox_Search);
            this.Search.Controls.Add(this.TxtBox_Search);
            this.Search.Location = new System.Drawing.Point(12, 12);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(672, 53);
            this.Search.TabIndex = 4;
            this.Search.TabStop = false;
            this.Search.Text = "Search";
            // 
            // Btn_Search
            // 
            this.Btn_Search.Location = new System.Drawing.Point(585, 18);
            this.Btn_Search.Name = "Btn_Search";
            this.Btn_Search.Size = new System.Drawing.Size(75, 21);
            this.Btn_Search.TabIndex = 7;
            this.Btn_Search.Text = "Search";
            this.Btn_Search.UseVisualStyleBackColor = true;
            this.Btn_Search.Click += new System.EventHandler(this.Btn_Search_Click);
            // 
            // CbBox_Search
            // 
            this.CbBox_Search.FormattingEnabled = true;
            this.CbBox_Search.Items.AddRange(new object[] {
            "Name",
            "Address",
            "Phone Number"});
            this.CbBox_Search.Location = new System.Drawing.Point(448, 17);
            this.CbBox_Search.Name = "CbBox_Search";
            this.CbBox_Search.Size = new System.Drawing.Size(127, 21);
            this.CbBox_Search.TabIndex = 6;
            // 
            // TxtBox_Search
            // 
            this.TxtBox_Search.Location = new System.Drawing.Point(9, 18);
            this.TxtBox_Search.Name = "TxtBox_Search";
            this.TxtBox_Search.Size = new System.Drawing.Size(433, 20);
            this.TxtBox_Search.TabIndex = 2;
            // 
            // CustomerList
            // 
            this.CustomerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CustomerList.Location = new System.Drawing.Point(9, 19);
            this.CustomerList.MultiSelect = false;
            this.CustomerList.Name = "CustomerList";
            this.CustomerList.ReadOnly = true;
            this.CustomerList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.CustomerList.Size = new System.Drawing.Size(425, 192);
            this.CustomerList.TabIndex = 5;
            this.CustomerList.SelectionChanged += new System.EventHandler(this.CustomerList_SelectionChanged);
            // 
            // groupBox_Options
            // 
            this.groupBox_Options.Controls.Add(this.Btn_Refresh);
            this.groupBox_Options.Controls.Add(this.Btn_Update);
            this.groupBox_Options.Location = new System.Drawing.Point(460, 302);
            this.groupBox_Options.Name = "groupBox_Options";
            this.groupBox_Options.Size = new System.Drawing.Size(225, 52);
            this.groupBox_Options.TabIndex = 7;
            this.groupBox_Options.TabStop = false;
            this.groupBox_Options.Text = "Controls";
            // 
            // Btn_Refresh
            // 
            this.Btn_Refresh.Location = new System.Drawing.Point(9, 19);
            this.Btn_Refresh.Name = "Btn_Refresh";
            this.Btn_Refresh.Size = new System.Drawing.Size(89, 26);
            this.Btn_Refresh.TabIndex = 13;
            this.Btn_Refresh.Text = "Refresh";
            this.Btn_Refresh.UseVisualStyleBackColor = true;
            this.Btn_Refresh.Click += new System.EventHandler(this.Btn_Refresh_Click);
            // 
            // Btn_Update
            // 
            this.Btn_Update.Location = new System.Drawing.Point(123, 20);
            this.Btn_Update.Name = "Btn_Update";
            this.Btn_Update.Size = new System.Drawing.Size(89, 26);
            this.Btn_Update.TabIndex = 12;
            this.Btn_Update.Text = "Update";
            this.Btn_Update.UseVisualStyleBackColor = true;
            this.Btn_Update.Click += new System.EventHandler(this.Btn_Update_Click);
            // 
            // groupBox_Details
            // 
            this.groupBox_Details.Controls.Add(this.TxtBox_Id);
            this.groupBox_Details.Controls.Add(this.label_Code);
            this.groupBox_Details.Controls.Add(this.TxtBox_Address);
            this.groupBox_Details.Controls.Add(this.label_Price);
            this.groupBox_Details.Controls.Add(this.label_Name);
            this.groupBox_Details.Controls.Add(this.TxtBox_PhoneNumber);
            this.groupBox_Details.Controls.Add(this.TxtBox_Name);
            this.groupBox_Details.Controls.Add(this.label_Producer);
            this.groupBox_Details.Location = new System.Drawing.Point(460, 72);
            this.groupBox_Details.Name = "groupBox_Details";
            this.groupBox_Details.Size = new System.Drawing.Size(224, 224);
            this.groupBox_Details.TabIndex = 8;
            this.groupBox_Details.TabStop = false;
            this.groupBox_Details.Text = "Details";
            // 
            // TxtBox_Id
            // 
            this.TxtBox_Id.Location = new System.Drawing.Point(61, 32);
            this.TxtBox_Id.Name = "TxtBox_Id";
            this.TxtBox_Id.ReadOnly = true;
            this.TxtBox_Id.Size = new System.Drawing.Size(151, 20);
            this.TxtBox_Id.TabIndex = 11;
            // 
            // label_Code
            // 
            this.label_Code.AutoSize = true;
            this.label_Code.Location = new System.Drawing.Point(6, 35);
            this.label_Code.Name = "label_Code";
            this.label_Code.Size = new System.Drawing.Size(19, 13);
            this.label_Code.TabIndex = 10;
            this.label_Code.Text = "Id:";
            // 
            // TxtBox_Address
            // 
            this.TxtBox_Address.Location = new System.Drawing.Point(61, 138);
            this.TxtBox_Address.Name = "TxtBox_Address";
            this.TxtBox_Address.Size = new System.Drawing.Size(151, 20);
            this.TxtBox_Address.TabIndex = 8;
            // 
            // label_Price
            // 
            this.label_Price.AutoSize = true;
            this.label_Price.Location = new System.Drawing.Point(6, 141);
            this.label_Price.Name = "label_Price";
            this.label_Price.Size = new System.Drawing.Size(48, 13);
            this.label_Price.TabIndex = 7;
            this.label_Price.Text = "Address:";
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Location = new System.Drawing.Point(6, 88);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(38, 13);
            this.label_Name.TabIndex = 2;
            this.label_Name.Text = "Name:";
            // 
            // TxtBox_PhoneNumber
            // 
            this.TxtBox_PhoneNumber.Location = new System.Drawing.Point(61, 191);
            this.TxtBox_PhoneNumber.Name = "TxtBox_PhoneNumber";
            this.TxtBox_PhoneNumber.Size = new System.Drawing.Size(151, 20);
            this.TxtBox_PhoneNumber.TabIndex = 1;
            // 
            // TxtBox_Name
            // 
            this.TxtBox_Name.Location = new System.Drawing.Point(61, 85);
            this.TxtBox_Name.Name = "TxtBox_Name";
            this.TxtBox_Name.Size = new System.Drawing.Size(151, 20);
            this.TxtBox_Name.TabIndex = 4;
            // 
            // label_Producer
            // 
            this.label_Producer.AutoSize = true;
            this.label_Producer.Location = new System.Drawing.Point(6, 194);
            this.label_Producer.Name = "label_Producer";
            this.label_Producer.Size = new System.Drawing.Size(41, 13);
            this.label_Producer.TabIndex = 3;
            this.label_Producer.Text = "Phone:";
            // 
            // Customer
            // 
            this.Customer.Controls.Add(this.CustomerList);
            this.Customer.Location = new System.Drawing.Point(12, 72);
            this.Customer.Name = "Customer";
            this.Customer.Size = new System.Drawing.Size(442, 224);
            this.Customer.TabIndex = 9;
            this.Customer.TabStop = false;
            this.Customer.Text = "Customer List";
            // 
            // Btn_Return
            // 
            this.Btn_Return.Location = new System.Drawing.Point(21, 321);
            this.Btn_Return.Name = "Btn_Return";
            this.Btn_Return.Size = new System.Drawing.Size(108, 26);
            this.Btn_Return.TabIndex = 10;
            this.Btn_Return.Text = "Return";
            this.Btn_Return.UseVisualStyleBackColor = true;
            this.Btn_Return.Click += new System.EventHandler(this.Btn_Return_Click);
            // 
            // CustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 366);
            this.Controls.Add(this.Btn_Return);
            this.Controls.Add(this.Customer);
            this.Controls.Add(this.groupBox_Details);
            this.Controls.Add(this.groupBox_Options);
            this.Controls.Add(this.Search);
            this.Name = "CustomerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Information";
            this.Load += new System.EventHandler(this.CustomerForm_Load);
            this.Search.ResumeLayout(false);
            this.Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerList)).EndInit();
            this.groupBox_Options.ResumeLayout(false);
            this.groupBox_Details.ResumeLayout(false);
            this.groupBox_Details.PerformLayout();
            this.Customer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Search;
        private System.Windows.Forms.Button Btn_Search;
        private System.Windows.Forms.ComboBox CbBox_Search;
        private System.Windows.Forms.TextBox TxtBox_Search;
        private System.Windows.Forms.DataGridView CustomerList;
        private System.Windows.Forms.GroupBox groupBox_Options;
        private System.Windows.Forms.Button Btn_Update;
        private System.Windows.Forms.GroupBox groupBox_Details;
        private System.Windows.Forms.TextBox TxtBox_Id;
        private System.Windows.Forms.Label label_Code;
        private System.Windows.Forms.TextBox TxtBox_Address;
        private System.Windows.Forms.Label label_Price;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.TextBox TxtBox_PhoneNumber;
        private System.Windows.Forms.TextBox TxtBox_Name;
        private System.Windows.Forms.Label label_Producer;
        private System.Windows.Forms.GroupBox Customer;
        private System.Windows.Forms.Button Btn_Return;
        private System.Windows.Forms.Button Btn_Refresh;
    }
}