﻿namespace FigureManagementSystem.GUI
{
    partial class InventoryReportFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nud_year = new System.Windows.Forms.NumericUpDown();
            this.btn_Show = new System.Windows.Forms.Button();
            this.tb_Total = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.February = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_April = new System.Windows.Forms.TextBox();
            this.tb_May = new System.Windows.Forms.TextBox();
            this.tb_June = new System.Windows.Forms.TextBox();
            this.tb_August = new System.Windows.Forms.TextBox();
            this.tb_September = new System.Windows.Forms.TextBox();
            this.tb_October = new System.Windows.Forms.TextBox();
            this.tb_November = new System.Windows.Forms.TextBox();
            this.tb_December = new System.Windows.Forms.TextBox();
            this.tb_February = new System.Windows.Forms.TextBox();
            this.tb_March = new System.Windows.Forms.TextBox();
            this.tb_July = new System.Windows.Forms.TextBox();
            this.tb_January = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nud_year)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // nud_year
            // 
            this.nud_year.Location = new System.Drawing.Point(52, 168);
            this.nud_year.Maximum = new decimal(new int[] {
            2020,
            0,
            0,
            0});
            this.nud_year.Minimum = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.nud_year.Name = "nud_year";
            this.nud_year.Size = new System.Drawing.Size(75, 20);
            this.nud_year.TabIndex = 3;
            this.nud_year.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nud_year.Value = new decimal(new int[] {
            2019,
            0,
            0,
            0});
            // 
            // btn_Show
            // 
            this.btn_Show.Location = new System.Drawing.Point(395, 166);
            this.btn_Show.Name = "btn_Show";
            this.btn_Show.Size = new System.Drawing.Size(75, 23);
            this.btn_Show.TabIndex = 4;
            this.btn_Show.Text = "Show";
            this.btn_Show.UseVisualStyleBackColor = true;
            this.btn_Show.Click += new System.EventHandler(this.Btn_Show_Click);
            // 
            // tb_Total
            // 
            this.tb_Total.Location = new System.Drawing.Point(223, 168);
            this.tb_Total.Name = "tb_Total";
            this.tb_Total.ReadOnly = true;
            this.tb_Total.Size = new System.Drawing.Size(75, 20);
            this.tb_Total.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.February);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.tb_April);
            this.groupBox2.Controls.Add(this.tb_May);
            this.groupBox2.Controls.Add(this.tb_June);
            this.groupBox2.Controls.Add(this.tb_August);
            this.groupBox2.Controls.Add(this.tb_September);
            this.groupBox2.Controls.Add(this.tb_October);
            this.groupBox2.Controls.Add(this.tb_November);
            this.groupBox2.Controls.Add(this.tb_December);
            this.groupBox2.Controls.Add(this.tb_February);
            this.groupBox2.Controls.Add(this.tb_March);
            this.groupBox2.Controls.Add(this.tb_July);
            this.groupBox2.Controls.Add(this.tb_January);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.btn_Refresh);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.tb_Total);
            this.groupBox2.Controls.Add(this.nud_year);
            this.groupBox2.Controls.Add(this.btn_Show);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(702, 212);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Total Income";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Month:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(626, 88);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 33;
            this.label15.Text = "December";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(520, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "November";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(425, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "October";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(306, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "September";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(218, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "August";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(127, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "July";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(652, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "June";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(549, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "May";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(443, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "April";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(327, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "March";
            // 
            // February
            // 
            this.February.AutoSize = true;
            this.February.Location = new System.Drawing.Point(210, 21);
            this.February.Name = "February";
            this.February.Size = new System.Drawing.Size(48, 13);
            this.February.TabIndex = 23;
            this.February.Text = "February";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(108, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "January";
            // 
            // tb_April
            // 
            this.tb_April.Location = new System.Drawing.Point(370, 48);
            this.tb_April.Name = "tb_April";
            this.tb_April.ReadOnly = true;
            this.tb_April.Size = new System.Drawing.Size(100, 20);
            this.tb_April.TabIndex = 21;
            // 
            // tb_May
            // 
            this.tb_May.Location = new System.Drawing.Point(476, 48);
            this.tb_May.Name = "tb_May";
            this.tb_May.ReadOnly = true;
            this.tb_May.Size = new System.Drawing.Size(100, 20);
            this.tb_May.TabIndex = 20;
            // 
            // tb_June
            // 
            this.tb_June.Location = new System.Drawing.Point(582, 48);
            this.tb_June.Name = "tb_June";
            this.tb_June.ReadOnly = true;
            this.tb_June.Size = new System.Drawing.Size(100, 20);
            this.tb_June.TabIndex = 19;
            // 
            // tb_August
            // 
            this.tb_August.Location = new System.Drawing.Point(158, 117);
            this.tb_August.Name = "tb_August";
            this.tb_August.ReadOnly = true;
            this.tb_August.Size = new System.Drawing.Size(100, 20);
            this.tb_August.TabIndex = 18;
            // 
            // tb_September
            // 
            this.tb_September.Location = new System.Drawing.Point(264, 117);
            this.tb_September.Name = "tb_September";
            this.tb_September.ReadOnly = true;
            this.tb_September.Size = new System.Drawing.Size(100, 20);
            this.tb_September.TabIndex = 17;
            // 
            // tb_October
            // 
            this.tb_October.Location = new System.Drawing.Point(370, 117);
            this.tb_October.Name = "tb_October";
            this.tb_October.ReadOnly = true;
            this.tb_October.Size = new System.Drawing.Size(100, 20);
            this.tb_October.TabIndex = 16;
            // 
            // tb_November
            // 
            this.tb_November.Location = new System.Drawing.Point(476, 117);
            this.tb_November.Name = "tb_November";
            this.tb_November.ReadOnly = true;
            this.tb_November.Size = new System.Drawing.Size(100, 20);
            this.tb_November.TabIndex = 15;
            // 
            // tb_December
            // 
            this.tb_December.Location = new System.Drawing.Point(582, 117);
            this.tb_December.Name = "tb_December";
            this.tb_December.ReadOnly = true;
            this.tb_December.Size = new System.Drawing.Size(100, 20);
            this.tb_December.TabIndex = 14;
            // 
            // tb_February
            // 
            this.tb_February.Location = new System.Drawing.Point(158, 48);
            this.tb_February.Name = "tb_February";
            this.tb_February.ReadOnly = true;
            this.tb_February.Size = new System.Drawing.Size(100, 20);
            this.tb_February.TabIndex = 13;
            // 
            // tb_March
            // 
            this.tb_March.Location = new System.Drawing.Point(264, 48);
            this.tb_March.Name = "tb_March";
            this.tb_March.ReadOnly = true;
            this.tb_March.Size = new System.Drawing.Size(100, 20);
            this.tb_March.TabIndex = 12;
            // 
            // tb_July
            // 
            this.tb_July.Location = new System.Drawing.Point(52, 117);
            this.tb_July.Name = "tb_July";
            this.tb_July.ReadOnly = true;
            this.tb_July.Size = new System.Drawing.Size(100, 20);
            this.tb_July.TabIndex = 11;
            // 
            // tb_January
            // 
            this.tb_January.Location = new System.Drawing.Point(52, 48);
            this.tb_January.Name = "tb_January";
            this.tb_January.ReadOnly = true;
            this.tb_January.Size = new System.Drawing.Size(100, 20);
            this.tb_January.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(145, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Total Income:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Year:";
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Location = new System.Drawing.Point(552, 165);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(75, 22);
            this.btn_Refresh.TabIndex = 1;
            this.btn_Refresh.Text = "Refresh";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.Btn_Refresh_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Month:";
            // 
            // InventoryReportFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(729, 232);
            this.Controls.Add(this.groupBox2);
            this.Name = "InventoryReportFrom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sales report";
            this.Load += new System.EventHandler(this.InventoryReportFrom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nud_year)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.NumericUpDown nud_year;
        private System.Windows.Forms.Button btn_Show;
        private System.Windows.Forms.TextBox tb_Total;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label February;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_April;
        private System.Windows.Forms.TextBox tb_May;
        private System.Windows.Forms.TextBox tb_June;
        private System.Windows.Forms.TextBox tb_August;
        private System.Windows.Forms.TextBox tb_September;
        private System.Windows.Forms.TextBox tb_October;
        private System.Windows.Forms.TextBox tb_November;
        private System.Windows.Forms.TextBox tb_December;
        private System.Windows.Forms.TextBox tb_February;
        private System.Windows.Forms.TextBox tb_March;
        private System.Windows.Forms.TextBox tb_July;
        private System.Windows.Forms.TextBox tb_January;
        private System.Windows.Forms.Button btn_Refresh;
    }
}