﻿namespace FigureManagementSystem.GUI
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TxtBox_AddImage = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtBox_AddInfo = new System.Windows.Forms.TextBox();
            this.CbBox_AddManufacturer = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Num_AddQuantity = new System.Windows.Forms.NumericUpDown();
            this.Btn_Clear = new System.Windows.Forms.Button();
            this.TxtBox_AddScale = new System.Windows.Forms.TextBox();
            this.TxtBox_AddName = new System.Windows.Forms.TextBox();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBox_AddPrice = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Num_AddQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TxtBox_AddImage);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.TxtBox_AddInfo);
            this.groupBox3.Controls.Add(this.CbBox_AddManufacturer);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.Num_AddQuantity);
            this.groupBox3.Controls.Add(this.Btn_Clear);
            this.groupBox3.Controls.Add(this.TxtBox_AddScale);
            this.groupBox3.Controls.Add(this.TxtBox_AddName);
            this.groupBox3.Controls.Add(this.Btn_Add);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.TxtBox_AddPrice);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(312, 261);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Control";
            // 
            // TxtBox_AddImage
            // 
            this.TxtBox_AddImage.Location = new System.Drawing.Point(98, 148);
            this.TxtBox_AddImage.Name = "TxtBox_AddImage";
            this.TxtBox_AddImage.Size = new System.Drawing.Size(196, 20);
            this.TxtBox_AddImage.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Image:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Information:";
            // 
            // TxtBox_AddInfo
            // 
            this.TxtBox_AddInfo.Location = new System.Drawing.Point(98, 95);
            this.TxtBox_AddInfo.Name = "TxtBox_AddInfo";
            this.TxtBox_AddInfo.Size = new System.Drawing.Size(196, 20);
            this.TxtBox_AddInfo.TabIndex = 17;
            // 
            // CbBox_AddManufacturer
            // 
            this.CbBox_AddManufacturer.FormattingEnabled = true;
            this.CbBox_AddManufacturer.Items.AddRange(new object[] {
            "Bandai",
            "Daban",
            "Gao Gao (TT Hongli)",
            "MC Model"});
            this.CbBox_AddManufacturer.Location = new System.Drawing.Point(98, 174);
            this.CbBox_AddManufacturer.Name = "CbBox_AddManufacturer";
            this.CbBox_AddManufacturer.Size = new System.Drawing.Size(196, 21);
            this.CbBox_AddManufacturer.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Quantity:";
            // 
            // Num_AddQuantity
            // 
            this.Num_AddQuantity.Location = new System.Drawing.Point(98, 122);
            this.Num_AddQuantity.Name = "Num_AddQuantity";
            this.Num_AddQuantity.Size = new System.Drawing.Size(196, 20);
            this.Num_AddQuantity.TabIndex = 14;
            this.Num_AddQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Btn_Clear
            // 
            this.Btn_Clear.Location = new System.Drawing.Point(22, 212);
            this.Btn_Clear.Name = "Btn_Clear";
            this.Btn_Clear.Size = new System.Drawing.Size(105, 32);
            this.Btn_Clear.TabIndex = 13;
            this.Btn_Clear.Text = "Clear";
            this.Btn_Clear.UseVisualStyleBackColor = true;
            this.Btn_Clear.Click += new System.EventHandler(this.Btn_Clear_Click);
            // 
            // TxtBox_AddScale
            // 
            this.TxtBox_AddScale.Location = new System.Drawing.Point(98, 45);
            this.TxtBox_AddScale.Name = "TxtBox_AddScale";
            this.TxtBox_AddScale.Size = new System.Drawing.Size(196, 20);
            this.TxtBox_AddScale.TabIndex = 4;
            // 
            // TxtBox_AddName
            // 
            this.TxtBox_AddName.Location = new System.Drawing.Point(98, 19);
            this.TxtBox_AddName.Name = "TxtBox_AddName";
            this.TxtBox_AddName.Size = new System.Drawing.Size(196, 20);
            this.TxtBox_AddName.TabIndex = 3;
            // 
            // Btn_Add
            // 
            this.Btn_Add.Location = new System.Drawing.Point(189, 212);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(105, 32);
            this.Btn_Add.TabIndex = 9;
            this.Btn_Add.Text = "Add";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Price:";
            // 
            // TxtBox_AddPrice
            // 
            this.TxtBox_AddPrice.Location = new System.Drawing.Point(98, 71);
            this.TxtBox_AddPrice.Name = "TxtBox_AddPrice";
            this.TxtBox_AddPrice.Size = new System.Drawing.Size(196, 20);
            this.TxtBox_AddPrice.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Manufacturer:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Scale:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 281);
            this.Controls.Add(this.groupBox3);
            this.Name = "AddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Figure";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Num_AddQuantity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button Btn_Clear;
        private System.Windows.Forms.TextBox TxtBox_AddScale;
        private System.Windows.Forms.TextBox TxtBox_AddName;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtBox_AddPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown Num_AddQuantity;
        private System.Windows.Forms.ComboBox CbBox_AddManufacturer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtBox_AddInfo;
        private System.Windows.Forms.TextBox TxtBox_AddImage;
        private System.Windows.Forms.Label label7;
    }
}