﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

using Firebase.Database;
using Firebase.Database.Query;

namespace FigureManagementSystem.GUI
{
    public partial class LoginForm : Form
    {
        private const String FIREBASE_APP = "https://figure-management.firebaseio.com/";
        private FirebaseClient firebase;

        public LoginForm()
        {
            InitializeComponent();
            InitFirebase();
            InitForm();
        }

        private void InitFirebase()
        {
            firebase = new FirebaseClient(FIREBASE_APP);
        }
        
        private void InitForm()
        {
            Txtbox_Password.PasswordChar = '*';
        }

        private async void Btn_Login_Click(object sender, EventArgs e)
        {
            bool result = false;
            List<Account> accounts = new List<Account>();
            var fbAccounts = await firebase.Child("accounts").OnceAsync<Account>();            
            foreach (var item in fbAccounts)
            {
                if (item.Object.Username.Equals(Txtbox_UserName.Text.Trim()) 
                    && item.Object.Password.Equals(Txtbox_Password.Text.Trim()))
                {
                    result = true;
                    break;
                }
            }
            if (result)
            {
                MessageBox.Show("Login successfully");
                this.Hide();
                new MainForm().Show();
            }
            else
                MessageBox.Show("Invalid account! Your user name or password may be wrong.");
        } 

        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            Txtbox_UserName.Text = "";
            Txtbox_Password.Text = "";
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
