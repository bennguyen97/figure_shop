﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FigureManagementSystem.GUI
{
    public partial class CheckOrderForm : Form
    {
        public CheckOrderForm()
        {
            InitializeComponent();
        }

        private void CheckOrderForm_Load(object sender, EventArgs e)
        {
            DisplayListOrder();
        }

        private void DisplayListOrder()
        {
            string url = "http://www.figuresoap.somee.com/api/FigureService/GetAllOrder";
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Orders> orders = JsonConvert.DeserializeObject<List<Orders>>(json);
            LoadOrdersToListOrder(orders);
        }

        private void LoadOrdersToListOrder(List<Orders> orders)
        {
            ListOrder.BeginInvoke(new MethodInvoker(delegate
            {
                ListOrder.DataSource = orders;
                ListOrder.Refresh();

                ListOrder.Columns["Id"].HeaderText = "Order Id";
                ListOrder.Columns["Id"].ReadOnly = true;
                ListOrder.Columns["Id"].Width = 50;

                ListOrder.Columns["OrderDate"].HeaderText = "Order date (mm/dd/yyyy)";
                ListOrder.Columns["OrderDate"].ReadOnly = true;
                ListOrder.Columns["OrderDate"].Width = 70;

                ListOrder.Columns["State"].HeaderText = "Order State";
                ListOrder.Columns["State"].ReadOnly = true;
                ListOrder.Columns["State"].Width = 70;

                ListOrder.Columns["Total"].HeaderText = "Total";
                ListOrder.Columns["Total"].ReadOnly = true;
                ListOrder.Columns["Total"].Width = 60;
                
                ListOrder.Columns["CustomerID"].Visible = false;
            }));
        }

        private void ListOrder_SelectionChanged(object sender, EventArgs e)
        {
            if (ListOrder.SelectedRows.Count > 0)
            {
                int ordersId = int.Parse(ListOrder.SelectedRows[0].Cells["Id"].Value.ToString());
                int customerId = int.Parse(ListOrder.SelectedRows[0].Cells["CustomerID"].Value.ToString());
                DisplayProductListOrder(ordersId);
                DisplayCustomerList(customerId);
            }
        }

        private void DisplayProductListOrder(int ordersId)
        {
            string url = "http://www.figuresoap.somee.com/api/FigureService/ShowOrderProductList?OrdersId=" + ordersId ;
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Receipt> receipts = JsonConvert.DeserializeObject<List<Receipt>>(json);
            LoadProductFromListOrder(receipts);
        }

        private void LoadProductFromListOrder(List<Receipt> receipts)
        {
            ProductOrderList.BeginInvoke(new MethodInvoker(delegate
            {
                ProductOrderList.DataSource = receipts;
                ProductOrderList.Refresh();

                ProductOrderList.Columns["Name"].HeaderText = "Product Name";
                ProductOrderList.Columns["Name"].ReadOnly = true;
                ProductOrderList.Columns["Name"].Width = 200;

                ProductOrderList.Columns["Price"].Visible = false;

                ProductOrderList.Columns["Quantity"].HeaderText = "Quantity";
                ProductOrderList.Columns["Quantity"].ReadOnly = true;
                ProductOrderList.Columns["Quantity"].Width = 50;
            }));
        }
        
        private void DisplayCustomerList(int customerId)
        {
            string url = "http://www.figuresoap.somee.com/api/FigureService/SearchCustomer?info=" + customerId + "&colname=id";
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Customer> customers = JsonConvert.DeserializeObject<List<Customer>>(json);
            LoadCustomerFromListOrder(customers);
        }

        private void LoadCustomerFromListOrder(List<Customer> customer)
        {
            CustomerOrderList.DataSource = customer;
            CustomerOrderList.Refresh();

            CustomerOrderList.Columns["Id"].Visible = false;

            CustomerOrderList.Columns["Name"].HeaderText = "Name";
            CustomerOrderList.Columns["Name"].ReadOnly = true;
            CustomerOrderList.Columns["Name"].Width = 104;

            CustomerOrderList.Columns["Address"].HeaderText = "Address";
            CustomerOrderList.Columns["Address"].ReadOnly = true;
            CustomerOrderList.Columns["Address"].Width = 100;

            CustomerOrderList.Columns["PhoneNumber"].HeaderText = "Phone Number";
            CustomerOrderList.Columns["PhoneNumber"].ReadOnly = true;
            CustomerOrderList.Columns["PhoneNumber"].Width = 90;
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            UpdateOrderState("cancel");

        }

        private void Btn_Success_Click(object sender, EventArgs e)
        {
            UpdateOrderState("success");
        }

        private void UpdateOrderState(string state)
        {
            int OrderId = int.Parse(ListOrder.SelectedRows[0].Cells["Id"].Value.ToString());
            Orders orders = new Orders() { Id = OrderId, State = state };
            String data = JsonConvert.SerializeObject(orders);

            string url = "http://www.figuresoap.somee.com/api/FigureService/UpdateOrder";
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            bool result = bool.Parse(client.UploadString(new Uri(url), data));

            if (result)
            {
                MessageBox.Show("Updated successfully!!!");
                DisplayListOrder();
            }
            else
                MessageBox.Show("Updating failed!!!");
        }
        
        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            DisplayListOrder();
        }

        private void Btn_StatePending_Click(object sender, EventArgs e)
        {
            ChangeOrderState("http://www.figuresoap.somee.com/api/FigureService/SearchOrder?info=pending&colName=State");
        }

        private void Btn_StateSuccess_Click(object sender, EventArgs e)
        {
            ChangeOrderState("http://www.figuresoap.somee.com/api/FigureService/SearchOrder?info=success&colName=State");
        }

        private void Btn_StateCancel_Click(object sender, EventArgs e)
        {
            ChangeOrderState("http://www.figuresoap.somee.com/api/FigureService/SearchOrder?info=cancel&colName=State");

        }

        private void ChangeOrderState(string url)
        {
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Orders> orders = JsonConvert.DeserializeObject<List<Orders>>(json);
            LoadOrdersToListOrder(orders);
        }
        
        private void Btn_Return_Click(object sender, EventArgs e)
        {
            new MainForm().Show();
            this.Hide();
        }
    }
}
