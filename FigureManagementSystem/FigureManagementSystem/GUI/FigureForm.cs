﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;

namespace FigureManagementSystem.GUI
{
    public partial class FigureForm : Form
    {
        private int code = 0;
        private AddForm addForm;

        public FigureForm()
        {
            InitializeComponent();
        }

        private void FigureForm_Load(object sender, EventArgs e)
        {
            DisplayTable();
            CbBox_Search.SelectedIndex = 0;
        }

        private void DisplayTable()
        {
            string url = "http://www.figuresoap.somee.com/api/FigureService/GetAllFigure";
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Figure> figures = JsonConvert.DeserializeObject<List<Figure>>(json);
            LoadFiguresDetailed(figures);
        }

        private void LoadFiguresDetailed(List<Figure> figs)
        {
            ProductList.BeginInvoke(new MethodInvoker(delegate
            {
                ProductList.DataSource = figs;
                ProductList.Refresh();

                ProductList.Columns["Code"].HeaderText = "Code";
                ProductList.Columns["Code"].ReadOnly = true;
                ProductList.Columns["Code"].Width = 50;

                ProductList.Columns["Name"].HeaderText = "Name";
                ProductList.Columns["Name"].ReadOnly = true;
                ProductList.Columns["Name"].Width = 150;

                ProductList.Columns["Scale"].HeaderText = "Scale";
                ProductList.Columns["Scale"].ReadOnly = true;
                ProductList.Columns["Scale"].Width = 100;

                ProductList.Columns["Price"].HeaderText = "Price";
                ProductList.Columns["Price"].ReadOnly = true;
                ProductList.Columns["Price"].Width = 90;

                ProductList.Columns["Info"].HeaderText = "Information";
                ProductList.Columns["Info"].ReadOnly = true;
                ProductList.Columns["Info"].Width = 305;

                ProductList.Columns["Quantity"].HeaderText = "Quantity";
                ProductList.Columns["Quantity"].ReadOnly = true;
                ProductList.Columns["Quantity"].Width = 50;

                ProductList.Columns["Image"].HeaderText = "Image";
                ProductList.Columns["Image"].ReadOnly = true;
                ProductList.Columns["Image"].Width = 50;

                ProductList.Columns["Manufacturer"].HeaderText = "Manufacturer";
                ProductList.Columns["Manufacturer"].ReadOnly = true;
                ProductList.Columns["Manufacturer"].Width = 100;
            }));
        }
        
        private void ProductList_SelectionChanged(object sender, EventArgs e)
        {
            if (ProductList.SelectedRows.Count > 0)
            {
                code = int.Parse(ProductList.SelectedRows[0].Cells["Code"].Value.ToString());
                TxtBox_Name.Text = ProductList.SelectedRows[0].Cells["Name"].Value.ToString();
                TxtBox_Scale.Text = ProductList.SelectedRows[0].Cells["Scale"].Value.ToString();
                TxtBox_Price.Text = ProductList.SelectedRows[0].Cells["Price"].Value.ToString();
                TxtBox_Info.Text = ProductList.SelectedRows[0].Cells["Info"].Value.ToString();
                TxtBox_Quantity.Text = ProductList.SelectedRows[0].Cells["Quantity"].Value.ToString();
                TxtBox_Image.Text = ProductList.SelectedRows[0].Cells["Image"].Value.ToString();
                CbBox_Manufacturer.Text = ProductList.SelectedRows[0].Cells["Manufacturer"].Value.ToString();
            }
        }

        private void RefreshTable()
        {
            DisplayTable();
        }
        
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if(addForm == null)
            {
                addForm = new AddForm(this);
            }
            addForm.Show();
        }
        
        public void AddProduct(Figure fig)
        {
            String data = JsonConvert.SerializeObject(fig);

            string url = "http://www.figuresoap.somee.com/api/FigureService/AddFigure";
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            bool result = bool.Parse(client.UploadString(new Uri(url), data));

            if (result)
            {
                MessageBox.Show("Adding successfully!!!");
                RefreshTable();
            }
            else
                MessageBox.Show("Adding failed!!!");
        }

        private void Btn_Update_Click(object sender, EventArgs e)
        {
            if (TxtBox_Name.Text != "" && TxtBox_Scale.Text != "" && TxtBox_Price.Text != "" 
                && TxtBox_Info.Text != "" && TxtBox_Quantity.Text != "")
            {
                String name = TxtBox_Name.Text.Trim();
                String scale = TxtBox_Scale.Text.Trim();
                int price = int.Parse(TxtBox_Price.Text.Trim());
                String info = TxtBox_Info.Text.Trim();
                int quantity = int.Parse(TxtBox_Quantity.Text.ToString());
                String image = TxtBox_Image.Text.Trim();
                String manufacturer = CbBox_Manufacturer.Text.ToString();
                Figure fig = new Figure() { Code = this.code, Name = name, Scale = scale, Price = price, Quantity = quantity, Info = info, Image = image, Manufacturer = manufacturer };
                String data = JsonConvert.SerializeObject(fig);

                string url = "http://www.figuresoap.somee.com/api/FigureService/UpdateFigure";
                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                bool result = bool.Parse(client.UploadString(new Uri(url), data));

                if (result)
                {
                    MessageBox.Show("Updating successfully!!!");
                    RefreshTable();
                }
                else
                    MessageBox.Show("Updating failed!!!");
            }
            else
            {
                MessageBox.Show("Please select row to update!!!");
            }
        }

        private void Btn_Delete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Choose wisely please :D", "Do you really need to delete?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if (TxtBox_Name.Text != "" && TxtBox_Scale.Text != "" && TxtBox_Price.Text != ""
                && TxtBox_Info.Text != "" && TxtBox_Quantity.Text != "")
                {
                    Figure fig = new Figure() { Code = this.code};
                    String data = JsonConvert.SerializeObject(fig);

                    string url = "http://www.figuresoap.somee.com/api/FigureService/DeleteFigure";
                    WebClient client = new WebClient();
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    bool result = bool.Parse(client.UploadString(new Uri(url), data));

                    if (result)
                    {
                        MessageBox.Show("Deleting successfully!!!");
                        RefreshTable();
                    }
                    else
                        MessageBox.Show("Deleting failed!!!");
                }
                else
                {
                    MessageBox.Show("Please select row to update!!!");
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                MessageBox.Show("Stop deleting!!!");
            }
        }        

        private void Btn_SearchFig_Click(object sender, EventArgs e)
        {
            RefreshTable();
            String info = TxtBox_Search.Text.Trim();
            int searchCol = CbBox_Search.SelectedIndex;
            if (TxtBox_Search.Text != "")
            {
                switch (searchCol)
                {
                    case 0:
                        {
                            ShowSearchResult("http://www.figuresoap.somee.com/api/FigureService/SearchFigure?info="
                                + info + "&colName=Name");
                        }
                        break;
                    case 1:
                        {
                            ShowSearchResult("http://www.figuresoap.somee.com/api/FigureService/SearchFigure?info="
                                + info + "&colName=Scale");
                        }
                        break;
                    case 2:
                        {
                            ShowSearchResult("http://www.figuresoap.somee.com/api/FigureService/SearchFigure?info="
                                + info + "&colName=Info");
                        }
                        break;
                    case 3:
                        {
                            ShowSearchResult("http://www.figuresoap.somee.com/api/FigureService/SearchFigure?info="
                                + info + "&colName=Manufacturer");
                        }
                        break;
                }
            }
            else
                MessageBox.Show("Please provide information to search!!!");
        }

        private void ShowSearchResult(string url)
        {
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Figure> figures = JsonConvert.DeserializeObject<List<Figure>>(json);
            LoadFiguresDetailed(figures);
        }
        
        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            RefreshTable();
        }

        private void Btn_Return_Click(object sender, EventArgs e)
        {
            new MainForm().Show();
            this.Hide();
        }
    }
}