﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FigureManagementSystem.GUI
{
    public partial class AddForm : Form
    {
        private FigureForm figForm;

        public AddForm(FigureForm figureForm)
        {
            figForm = figureForm;
            InitializeComponent();
            CbBox_AddManufacturer.SelectedIndex = 0;
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (TxtBox_AddName.Text != "" && TxtBox_AddScale.Text != "" && TxtBox_AddPrice.Text != "" && TxtBox_AddInfo.Text != "" && TxtBox_AddImage.Text != "" && Num_AddQuantity.Value != 0 && CbBox_AddManufacturer.Text !="")
            {
                String name = TxtBox_AddName.Text.Trim();
                String scale = TxtBox_AddScale.Text.Trim();
                int price = int.Parse(TxtBox_AddPrice.Text.Trim());
                String info = TxtBox_AddInfo.Text.Trim();
                int quantity = int.Parse(Num_AddQuantity.Value.ToString());
                String image = TxtBox_AddImage.Text.Trim();
                String manufacturer= CbBox_AddManufacturer.Text.ToString();
                Figure fig = new Figure() { Code = 0, Name = name, Scale = scale, Price = price, Info = info, Quantity = quantity, Image = image, Manufacturer =  manufacturer};
                figForm.AddProduct(fig);
                this.Hide();
                ClearData();
            }
            else
            {
                MessageBox.Show("Please provide all information to add new product!!!");
            }
        }

        private void Btn_Clear_Click(object sender, EventArgs e)
        {
            ClearData();
        }        
        private void ClearData()
        {
            TxtBox_AddName.Text = "";
            TxtBox_AddScale.Text = "";
            TxtBox_AddPrice.Text = "";
            TxtBox_AddInfo.Text = "";
            Num_AddQuantity.Value = 1;
            TxtBox_AddImage.Text = "";
            CbBox_AddManufacturer.SelectedIndex = 0;
        }
    }
}
