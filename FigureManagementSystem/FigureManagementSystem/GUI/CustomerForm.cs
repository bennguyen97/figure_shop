﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FigureManagementSystem.GUI
{
    public partial class CustomerForm : Form
    {
        int id = 0;
        public CustomerForm()
        {
            InitializeComponent();
        }

        private void CustomerForm_Load(object sender, EventArgs e)
        {
            DisplayTable();
            CbBox_Search.SelectedIndex = 0;
        }

        private void DisplayTable()
        {
            string url = "http://www.figuresoap.somee.com/api/FigureService/GetAllCustomer";
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Customer> customers = JsonConvert.DeserializeObject<List<Customer>>(json);
            LoadCustomerDetailed(customers);
        }

        private void LoadCustomerDetailed(List<Customer> customers)
        {
            CustomerList.BeginInvoke(new MethodInvoker(delegate
            {
                CustomerList.DataSource = customers;
                CustomerList.Refresh();

                CustomerList.Columns["Id"].HeaderText = "Id";
                CustomerList.Columns["Id"].ReadOnly = true;
                CustomerList.Columns["Id"].Width = 50;

                CustomerList.Columns["Name"].HeaderText = "Name";
                CustomerList.Columns["Name"].ReadOnly = true;
                CustomerList.Columns["Name"].Width = 130;

                CustomerList.Columns["Address"].HeaderText = "Address";
                CustomerList.Columns["Address"].ReadOnly = true;
                CustomerList.Columns["Address"].Width = 100;

                CustomerList.Columns["PhoneNumber"].HeaderText = "Phone Number";
                CustomerList.Columns["PhoneNumber"].ReadOnly = true;
                CustomerList.Columns["PhoneNumber"].Width = 100;
            }));
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        { 
            string info = TxtBox_Search.Text.Trim();
            int searchCol = CbBox_Search.SelectedIndex;
            if (TxtBox_Search.Text != "")
            {
                switch (searchCol)
                {
                    case 0:
                        {
                            ShowSearchResult("http://www.figuresoap.somee.com/api/FigureService/SearchCustomer?info="
                                + info + "&colName=Name");
                        }
                        break;
                    case 1:
                        {
                            ShowSearchResult("http://www.figuresoap.somee.com/api/FigureService/SearchCustomer?info="
                                + info + "&colName=Address");
                        }
                        break;
                    case 2:
                        {
                            ShowSearchResult("http://www.figuresoap.somee.com/api/FigureService/SearchCustomer?info="
                                + info + "&colName=PhoneNumber");
                        }
                        break;
                }
            }
            else
                MessageBox.Show("Please provide information to search!!!");
        }

        private void ShowSearchResult(string url)
        {
            WebClient client = new WebClient();
            string json = client.DownloadString(new Uri(url));
            List<Customer> customers = JsonConvert.DeserializeObject<List<Customer>>(json);
            LoadCustomerDetailed(customers);
        }

        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            DisplayTable();
        }

        private void Btn_Return_Click(object sender, EventArgs e)
        {
            new MainForm().Show();
            this.Hide();
        }

        private void Btn_Update_Click(object sender, EventArgs e)
        {
            if (TxtBox_Name.Text != "" && TxtBox_Address.Text != "" && TxtBox_PhoneNumber.Text != "")
            {
                String name = TxtBox_Name.Text.Trim();
                String address = TxtBox_Address.Text.Trim();
                String phoneNumber = TxtBox_PhoneNumber.Text.Trim();
                Customer customer = new Customer() { Id = this.id, Name = name, Address = address, PhoneNumber = phoneNumber };
                String data = JsonConvert.SerializeObject(customer);

                string url = "http://www.figuresoap.somee.com/api/FigureService/UpdateCustomer";
                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                bool result = bool.Parse(client.UploadString(new Uri(url), data));

                if (result)
                {
                    MessageBox.Show("Updating successfully!!!");
                    DisplayTable();
                }
                else
                    MessageBox.Show("Updating failed!!!");
            }
            else
            {
                MessageBox.Show("Please select row to update!!!");
            }
        }

        private void CustomerList_SelectionChanged(object sender, EventArgs e)
        {
            if (CustomerList.SelectedRows.Count > 0)
            {
                id = int.Parse(CustomerList.SelectedRows[0].Cells["Id"].Value.ToString());
                TxtBox_Id.Text = CustomerList.SelectedRows[0].Cells["Id"].Value.ToString();
                TxtBox_Name.Text = CustomerList.SelectedRows[0].Cells["Name"].Value.ToString();
                TxtBox_Address.Text = CustomerList.SelectedRows[0].Cells["Address"].Value.ToString();
                TxtBox_PhoneNumber.Text = CustomerList.SelectedRows[0].Cells["PhoneNumber"].Value.ToString();
            }
        }
    }
}
