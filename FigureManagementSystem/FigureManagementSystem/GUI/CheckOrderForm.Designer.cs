﻿namespace FigureManagementSystem.GUI
{
    partial class CheckOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Btn_StateCancel = new System.Windows.Forms.Button();
            this.Btn_StateSuccess = new System.Windows.Forms.Button();
            this.Btn_StatePending = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ListOrder = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.Btn_Success = new System.Windows.Forms.Button();
            this.Btn_Refresh = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ProductOrderList = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CustomerOrderList = new System.Windows.Forms.DataGridView();
            this.Btn_Return = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListOrder)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductOrderList)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerOrderList)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Btn_StateCancel);
            this.groupBox2.Controls.Add(this.Btn_StateSuccess);
            this.groupBox2.Controls.Add(this.Btn_StatePending);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.ListOrder);
            this.groupBox2.Location = new System.Drawing.Point(368, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(313, 230);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "List Order";
            // 
            // Btn_StateCancel
            // 
            this.Btn_StateCancel.Location = new System.Drawing.Point(236, 183);
            this.Btn_StateCancel.Name = "Btn_StateCancel";
            this.Btn_StateCancel.Size = new System.Drawing.Size(66, 32);
            this.Btn_StateCancel.TabIndex = 16;
            this.Btn_StateCancel.Text = "Cancel";
            this.Btn_StateCancel.UseVisualStyleBackColor = true;
            this.Btn_StateCancel.Click += new System.EventHandler(this.Btn_StateCancel_Click);
            // 
            // Btn_StateSuccess
            // 
            this.Btn_StateSuccess.Location = new System.Drawing.Point(163, 183);
            this.Btn_StateSuccess.Name = "Btn_StateSuccess";
            this.Btn_StateSuccess.Size = new System.Drawing.Size(66, 31);
            this.Btn_StateSuccess.TabIndex = 15;
            this.Btn_StateSuccess.Text = "Success";
            this.Btn_StateSuccess.UseVisualStyleBackColor = true;
            this.Btn_StateSuccess.Click += new System.EventHandler(this.Btn_StateSuccess_Click);
            // 
            // Btn_StatePending
            // 
            this.Btn_StatePending.Location = new System.Drawing.Point(91, 183);
            this.Btn_StatePending.Name = "Btn_StatePending";
            this.Btn_StatePending.Size = new System.Drawing.Size(66, 30);
            this.Btn_StatePending.TabIndex = 14;
            this.Btn_StatePending.Text = "Pending";
            this.Btn_StatePending.UseVisualStyleBackColor = true;
            this.Btn_StatePending.Click += new System.EventHandler(this.Btn_StatePending_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 192);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Order by state:";
            // 
            // ListOrder
            // 
            this.ListOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ListOrder.Location = new System.Drawing.Point(6, 18);
            this.ListOrder.MultiSelect = false;
            this.ListOrder.Name = "ListOrder";
            this.ListOrder.ReadOnly = true;
            this.ListOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ListOrder.Size = new System.Drawing.Size(298, 150);
            this.ListOrder.TabIndex = 0;
            this.ListOrder.SelectionChanged += new System.EventHandler(this.ListOrder_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(753, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Update Order State:";
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.Location = new System.Drawing.Point(931, 195);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.Size = new System.Drawing.Size(64, 32);
            this.Btn_Cancel.TabIndex = 8;
            this.Btn_Cancel.Text = "Cancel";
            this.Btn_Cancel.UseVisualStyleBackColor = true;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // Btn_Success
            // 
            this.Btn_Success.Location = new System.Drawing.Point(855, 195);
            this.Btn_Success.Name = "Btn_Success";
            this.Btn_Success.Size = new System.Drawing.Size(64, 32);
            this.Btn_Success.TabIndex = 10;
            this.Btn_Success.Text = "Success";
            this.Btn_Success.UseVisualStyleBackColor = true;
            this.Btn_Success.Click += new System.EventHandler(this.Btn_Success_Click);
            // 
            // Btn_Refresh
            // 
            this.Btn_Refresh.Location = new System.Drawing.Point(296, 195);
            this.Btn_Refresh.Name = "Btn_Refresh";
            this.Btn_Refresh.Size = new System.Drawing.Size(66, 32);
            this.Btn_Refresh.TabIndex = 12;
            this.Btn_Refresh.Text = "Refresh";
            this.Btn_Refresh.UseVisualStyleBackColor = true;
            this.Btn_Refresh.Click += new System.EventHandler(this.Btn_Refresh_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ProductOrderList);
            this.groupBox1.Location = new System.Drawing.Point(687, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(308, 178);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Product List";
            // 
            // ProductOrderList
            // 
            this.ProductOrderList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductOrderList.Location = new System.Drawing.Point(6, 18);
            this.ProductOrderList.MultiSelect = false;
            this.ProductOrderList.Name = "ProductOrderList";
            this.ProductOrderList.ReadOnly = true;
            this.ProductOrderList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProductOrderList.Size = new System.Drawing.Size(296, 150);
            this.ProductOrderList.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CustomerOrderList);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(350, 178);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Customer Information";
            // 
            // CustomerOrderList
            // 
            this.CustomerOrderList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CustomerOrderList.Location = new System.Drawing.Point(7, 19);
            this.CustomerOrderList.Name = "CustomerOrderList";
            this.CustomerOrderList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.CustomerOrderList.Size = new System.Drawing.Size(337, 149);
            this.CustomerOrderList.TabIndex = 0;
            // 
            // Btn_Return
            // 
            this.Btn_Return.Location = new System.Drawing.Point(12, 195);
            this.Btn_Return.Name = "Btn_Return";
            this.Btn_Return.Size = new System.Drawing.Size(84, 30);
            this.Btn_Return.TabIndex = 15;
            this.Btn_Return.Text = "Return";
            this.Btn_Return.UseVisualStyleBackColor = true;
            this.Btn_Return.Click += new System.EventHandler(this.Btn_Return_Click);
            // 
            // CheckOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 252);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Btn_Return);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.Btn_Refresh);
            this.Controls.Add(this.Btn_Success);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CheckOrderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order List";
            this.Load += new System.EventHandler(this.CheckOrderForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListOrder)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProductOrderList)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustomerOrderList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView ListOrder;
        private System.Windows.Forms.Button Btn_Cancel;
        private System.Windows.Forms.Button Btn_Success;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView ProductOrderList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_Refresh;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView CustomerOrderList;
        private System.Windows.Forms.Button Btn_StatePending;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Btn_Return;
        private System.Windows.Forms.Button Btn_StateSuccess;
        private System.Windows.Forms.Button Btn_StateCancel;
    }
}