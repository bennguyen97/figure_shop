﻿using FigureManagementSystem.DTO;
using FigureManagementSystem.BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FigureManagementSystem.GUI
{
    public partial class InventoryReportFrom : Form
    {
        public DateTime thisTime = DateTime.Now;
        int maxMonth = 0;

        public InventoryReportFrom()
        {
            InitializeComponent();
        }

        private void InventoryReportFrom_Load(object sender, EventArgs e)
        {
            int thisYear = thisTime.Year;
            nud_year.Maximum = thisTime.Year;
            nud_year.Value = thisYear;
            this.maxMonth = thisTime.Month;
            ShowIncome(thisYear, maxMonth);
        }

        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(nud_year.Value);
            ShowIncome(year, maxMonth);
        }

        private void Btn_Show_Click(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(nud_year.Value);
            ShowIncome(year, maxMonth);
        }

        private void ShowIncome(int year, int maxMonth)
        {            
            tb_January.Text = new InventoryBUS().CalculateIncome(1, 31, year).ToString();
            tb_February.Text = new InventoryBUS().CalculateIncome(2, 28, year).ToString();
            tb_March.Text = new InventoryBUS().CalculateIncome(3, 31, year).ToString();
            tb_April.Text = new InventoryBUS().CalculateIncome(4, 30, year).ToString();
            tb_May.Text = new InventoryBUS().CalculateIncome(5, 31, year).ToString();
            tb_June.Text = new InventoryBUS().CalculateIncome(6, 30, year).ToString();
            tb_July.Text = new InventoryBUS().CalculateIncome(7, 31, year).ToString();
            tb_August.Text = new InventoryBUS().CalculateIncome(8, 31, year).ToString();
            tb_September.Text = new InventoryBUS().CalculateIncome(9, 30, year).ToString();
            tb_October.Text = new InventoryBUS().CalculateIncome(10, 31, year).ToString();
            tb_November.Text = new InventoryBUS().CalculateIncome(11, 30, year).ToString();
            tb_December.Text = new InventoryBUS().CalculateIncome(12, 31, year).ToString();

            int total = int.Parse(tb_January.Text) + int.Parse(tb_February.Text) + int.Parse(tb_March.Text)
                + int.Parse(tb_April.Text) + int.Parse(tb_May.Text) + int.Parse(tb_June.Text)
                + int.Parse(tb_July.Text) + int.Parse(tb_August.Text) + int.Parse(tb_September.Text)
                + int.Parse(tb_October.Text) + int.Parse(tb_November.Text) + int.Parse(tb_December.Text);
            tb_Total.Text = total.ToString();
        }
    }
}
