﻿using FigureManagementSystem.BUS;
using FigureManagementSystem.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FigureManagementSystem.GUI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void checkOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CheckOrderForm().Show();
            this.Hide();
        }

        private void productListToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new FigureForm().Show();
            this.Hide();
        }

        private void customerListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CustomerForm().Show();
            this.Hide();
        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new InventoryReportFrom().Show();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (PreClosingConfirmation() == System.Windows.Forms.DialogResult.Yes)
            {
                Dispose(true);
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private DialogResult PreClosingConfirmation()
        {
            DialogResult res = System.Windows.Forms.MessageBox.Show(" Do you want to quit?          "
                , "Quit...", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            return res;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DisplayTable();
        }

        private void DisplayTable()
        {
            List<Inventory> inventories = new InventoryBUS().GetAllInventory();
            LoadInventoryDetailed(inventories);
        }

        private void LoadInventoryDetailed(List<Inventory> figs)
        {
            dgv_Inventory.BeginInvoke(new MethodInvoker(delegate
            {
                dgv_Inventory.DataSource = figs;
                dgv_Inventory.Refresh();

                dgv_Inventory.Columns["Code"].HeaderText = "Code";
                dgv_Inventory.Columns["Code"].ReadOnly = true;
                dgv_Inventory.Columns["Code"].Width = 50;

                dgv_Inventory.Columns["Name"].HeaderText = "Name";
                dgv_Inventory.Columns["Name"].ReadOnly = true;
                dgv_Inventory.Columns["Name"].Width = 300;

                dgv_Inventory.Columns["Stock"].HeaderText = "Stock";
                dgv_Inventory.Columns["Stock"].ReadOnly = true;
                dgv_Inventory.Columns["Stock"].Width = 50;

                dgv_Inventory.Columns["Sold"].HeaderText = "Sold";
                dgv_Inventory.Columns["Sold"].ReadOnly = true;
                dgv_Inventory.Columns["Sold"].Width = 50;
            }));
        }

        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            DisplayTable();
        }
    }
}
