﻿namespace FigureManagementSystem.GUI
{
    partial class FigureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProductList = new System.Windows.Forms.DataGridView();
            this.Search = new System.Windows.Forms.GroupBox();
            this.Btn_SearchFig = new System.Windows.Forms.Button();
            this.CbBox_Search = new System.Windows.Forms.ComboBox();
            this.TxtBox_Search = new System.Windows.Forms.TextBox();
            this.Btn_Search = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Btn_Refresh = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.Btn_Delete = new System.Windows.Forms.Button();
            this.Btn_Return = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TxtBox_Image = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtBox_Quantity = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CbBox_Manufacturer = new System.Windows.Forms.ComboBox();
            this.TxtBox_Price = new System.Windows.Forms.TextBox();
            this.TxtBox_Scale = new System.Windows.Forms.TextBox();
            this.TxtBox_Name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Btn_Update = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBox_Info = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ProductList)).BeginInit();
            this.Search.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProductList
            // 
            this.ProductList.Location = new System.Drawing.Point(6, 18);
            this.ProductList.Name = "ProductList";
            this.ProductList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProductList.Size = new System.Drawing.Size(955, 150);
            this.ProductList.TabIndex = 0;
            this.ProductList.SelectionChanged += new System.EventHandler(this.ProductList_SelectionChanged);
            // 
            // Search
            // 
            this.Search.Controls.Add(this.Btn_SearchFig);
            this.Search.Controls.Add(this.CbBox_Search);
            this.Search.Controls.Add(this.TxtBox_Search);
            this.Search.Location = new System.Drawing.Point(12, 12);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(967, 53);
            this.Search.TabIndex = 2;
            this.Search.TabStop = false;
            this.Search.Text = "Search";
            // 
            // Btn_SearchFig
            // 
            this.Btn_SearchFig.Location = new System.Drawing.Point(877, 12);
            this.Btn_SearchFig.Name = "Btn_SearchFig";
            this.Btn_SearchFig.Size = new System.Drawing.Size(84, 32);
            this.Btn_SearchFig.TabIndex = 2;
            this.Btn_SearchFig.Text = "Search";
            this.Btn_SearchFig.UseVisualStyleBackColor = true;
            this.Btn_SearchFig.Click += new System.EventHandler(this.Btn_SearchFig_Click);
            // 
            // CbBox_Search
            // 
            this.CbBox_Search.Items.AddRange(new object[] {
            "Name",
            "Scale",
            "Info",
            "Manufacturer"});
            this.CbBox_Search.Location = new System.Drawing.Point(750, 18);
            this.CbBox_Search.Name = "CbBox_Search";
            this.CbBox_Search.Size = new System.Drawing.Size(121, 21);
            this.CbBox_Search.TabIndex = 0;
            // 
            // TxtBox_Search
            // 
            this.TxtBox_Search.Location = new System.Drawing.Point(6, 19);
            this.TxtBox_Search.Name = "TxtBox_Search";
            this.TxtBox_Search.Size = new System.Drawing.Size(738, 20);
            this.TxtBox_Search.TabIndex = 1;
            // 
            // Btn_Search
            // 
            this.Btn_Search.Location = new System.Drawing.Point(0, 0);
            this.Btn_Search.Name = "Btn_Search";
            this.Btn_Search.Size = new System.Drawing.Size(75, 23);
            this.Btn_Search.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Btn_Refresh);
            this.groupBox2.Controls.Add(this.ProductList);
            this.groupBox2.Controls.Add(this.Btn_Add);
            this.groupBox2.Controls.Add(this.Btn_Delete);
            this.groupBox2.Controls.Add(this.Btn_Return);
            this.groupBox2.Location = new System.Drawing.Point(12, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(967, 212);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Figure List";
            // 
            // Btn_Refresh
            // 
            this.Btn_Refresh.Location = new System.Drawing.Point(697, 174);
            this.Btn_Refresh.Name = "Btn_Refresh";
            this.Btn_Refresh.Size = new System.Drawing.Size(84, 32);
            this.Btn_Refresh.TabIndex = 11;
            this.Btn_Refresh.Text = "Refresh";
            this.Btn_Refresh.UseVisualStyleBackColor = true;
            this.Btn_Refresh.Click += new System.EventHandler(this.Btn_Refresh_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Location = new System.Drawing.Point(787, 174);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(84, 32);
            this.Btn_Add.TabIndex = 8;
            this.Btn_Add.Text = "Add New...";
            this.Btn_Add.UseVisualStyleBackColor = true;
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // Btn_Delete
            // 
            this.Btn_Delete.Location = new System.Drawing.Point(877, 174);
            this.Btn_Delete.Name = "Btn_Delete";
            this.Btn_Delete.Size = new System.Drawing.Size(84, 32);
            this.Btn_Delete.TabIndex = 10;
            this.Btn_Delete.Text = "Delete";
            this.Btn_Delete.UseVisualStyleBackColor = true;
            this.Btn_Delete.Click += new System.EventHandler(this.Btn_Delete_Click);
            // 
            // Btn_Return
            // 
            this.Btn_Return.Location = new System.Drawing.Point(6, 174);
            this.Btn_Return.Name = "Btn_Return";
            this.Btn_Return.Size = new System.Drawing.Size(84, 32);
            this.Btn_Return.TabIndex = 18;
            this.Btn_Return.Text = "Return";
            this.Btn_Return.Click += new System.EventHandler(this.Btn_Return_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TxtBox_Image);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.TxtBox_Quantity);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.CbBox_Manufacturer);
            this.groupBox3.Controls.Add(this.TxtBox_Price);
            this.groupBox3.Controls.Add(this.TxtBox_Scale);
            this.groupBox3.Controls.Add(this.TxtBox_Name);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.Btn_Update);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.TxtBox_Info);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(985, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(312, 271);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Details";
            // 
            // TxtBox_Image
            // 
            this.TxtBox_Image.Location = new System.Drawing.Point(93, 159);
            this.TxtBox_Image.Name = "TxtBox_Image";
            this.TxtBox_Image.Size = new System.Drawing.Size(206, 20);
            this.TxtBox_Image.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Image:";
            // 
            // TxtBox_Quantity
            // 
            this.TxtBox_Quantity.Location = new System.Drawing.Point(93, 131);
            this.TxtBox_Quantity.Name = "TxtBox_Quantity";
            this.TxtBox_Quantity.Size = new System.Drawing.Size(206, 20);
            this.TxtBox_Quantity.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Quantity:";
            // 
            // CbBox_Manufacturer
            // 
            this.CbBox_Manufacturer.FormattingEnabled = true;
            this.CbBox_Manufacturer.Items.AddRange(new object[] {
            "Bandai",
            "Daban",
            "Gao Gao (TT Hongli)",
            "MC Model"});
            this.CbBox_Manufacturer.Location = new System.Drawing.Point(93, 187);
            this.CbBox_Manufacturer.Name = "CbBox_Manufacturer";
            this.CbBox_Manufacturer.Size = new System.Drawing.Size(206, 21);
            this.CbBox_Manufacturer.TabIndex = 17;
            // 
            // TxtBox_Price
            // 
            this.TxtBox_Price.Location = new System.Drawing.Point(92, 79);
            this.TxtBox_Price.Name = "TxtBox_Price";
            this.TxtBox_Price.Size = new System.Drawing.Size(207, 20);
            this.TxtBox_Price.TabIndex = 4;
            // 
            // TxtBox_Scale
            // 
            this.TxtBox_Scale.Location = new System.Drawing.Point(92, 53);
            this.TxtBox_Scale.Name = "TxtBox_Scale";
            this.TxtBox_Scale.Size = new System.Drawing.Size(207, 20);
            this.TxtBox_Scale.TabIndex = 3;
            // 
            // TxtBox_Name
            // 
            this.TxtBox_Name.Location = new System.Drawing.Point(92, 27);
            this.TxtBox_Name.Name = "TxtBox_Name";
            this.TxtBox_Name.Size = new System.Drawing.Size(207, 20);
            this.TxtBox_Name.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Information:";
            // 
            // Btn_Update
            // 
            this.Btn_Update.Location = new System.Drawing.Point(222, 233);
            this.Btn_Update.Name = "Btn_Update";
            this.Btn_Update.Size = new System.Drawing.Size(84, 32);
            this.Btn_Update.TabIndex = 9;
            this.Btn_Update.Text = "Update";
            this.Btn_Update.UseVisualStyleBackColor = true;
            this.Btn_Update.Click += new System.EventHandler(this.Btn_Update_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Price:";
            // 
            // TxtBox_Info
            // 
            this.TxtBox_Info.Location = new System.Drawing.Point(92, 105);
            this.TxtBox_Info.Name = "TxtBox_Info";
            this.TxtBox_Info.Size = new System.Drawing.Size(207, 20);
            this.TxtBox_Info.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Manufacturer:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Scale:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // FigureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1309, 294);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Search);
            this.Name = "FigureForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Figure Management System";
            this.Load += new System.EventHandler(this.FigureForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProductList)).EndInit();
            this.Search.ResumeLayout(false);
            this.Search.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ProductList;
        private System.Windows.Forms.GroupBox Search;
        private System.Windows.Forms.TextBox TxtBox_Search;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button Btn_Delete;
        private System.Windows.Forms.Button Btn_Update;
        private System.Windows.Forms.Button Btn_Add;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtBox_Info;
        private System.Windows.Forms.TextBox TxtBox_Price;
        private System.Windows.Forms.TextBox TxtBox_Scale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtBox_Name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CbBox_Search;
        private System.Windows.Forms.ComboBox CbBox_Manufacturer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtBox_Quantity;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Btn_Search;
        private System.Windows.Forms.Button Btn_Refresh;
        private System.Windows.Forms.Button Btn_SearchFig;
        private System.Windows.Forms.TextBox TxtBox_Image;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Btn_Return;
    }
}

