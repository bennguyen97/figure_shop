﻿using FigureManagementSystem.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FigureManagementSystem.DAL
{
    class InventoryDAO
    {
        public List<Inventory> SelectAllInventory()
        {
            try
            {
            List<Inventory> figures = new List<Inventory>();
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "Select code, name, figure.quantity as stock, sum(order_detailed.quantity) as sold " +
                "From figure, order_detailed, orders " +
                "Where(figure.code = order_detailed.product_id) AND(order_detailed.order_id = orders.id)  AND(orders.state = 'success') " +
                "Group by code, name, figure.quantity";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Inventory fig = new Inventory()
                {
                    Code = (int)dr["code"],
                    Name = (String)dr["name"],
                    Stock = (int)dr["stock"],
                    Sold = (int)dr["sold"],
                };
                figures.Add(fig);
            }
            return figures;
            }
            catch
            {
                return null;
            }
        }

        public int TotalIncome(int month, int date, int year)
        {
            try
            {
                int income = 0;
                String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
                SqlConnection con = new SqlConnection(strCon);
                con.Open();
                String strCom = "SELECT sum(total) as income " +
                    "FROM orders " +
                    "WHERE (order_date >= '" + month + "/1/" + year + "') AND (order_date <= '" + month + "/" + date + "/" + year + "') AND (state = 'success')";
                SqlCommand com = new SqlCommand(strCom, con);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    income = (int)dr["income"];
                }
                return income;

            }
            catch(Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                return 0;
            }
        }
    }
}
