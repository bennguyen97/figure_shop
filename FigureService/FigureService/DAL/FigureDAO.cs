﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FigureService.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace FigureService.DAL
{
    public class FigureDAO
    {
        public List<Figure> SelectAllFigure()
        {
            try
            {
            List<Figure> figures = new List<Figure>();
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT * FROM FIGURE";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Figure fig = new Figure()
                {
                    Code = (int)dr["code"],
                    Name = (String)dr["name"],
                    Scale = (String)dr["scale"],
                    Price = (int)dr["price"],
                    Info = (String)dr["info"],
                    Quantity = (int)dr["quantity"],
                    Image = (String)dr["image"],
                    Manufacturer = (String)dr["manufacturer"],
                };
                figures.Add(fig);
            }
            return figures;                
            }
            catch
            {
                return null;
            }
        }

        public bool InsertFigure(Figure fig)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "INSERT INTO FIGURE VALUES(@name, @scale, @price, @info, @quantity, @image, @manufacturer)";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@name", fig.Name));
            com.Parameters.Add(new SqlParameter("@scale", fig.Scale));
            com.Parameters.Add(new SqlParameter("@price", fig.Price));
            com.Parameters.Add(new SqlParameter("@info", fig.Info));
            com.Parameters.Add(new SqlParameter("@quantity", fig.Quantity));
            com.Parameters.Add(new SqlParameter("@image", fig.Image));
            com.Parameters.Add(new SqlParameter("@manufacturer", fig.Manufacturer));
            return com.ExecuteNonQuery() > 0;
        }
        
        public bool UpdateFigure(Figure fig)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "UPDATE FIGURE SET name = @name, scale = @scale, price = @price, " +
                "info = @info, quantity = @quantity, image = @image, manufacturer = @manufacturer WHERE Code = @Code";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@Code", fig.Code));
            com.Parameters.Add(new SqlParameter("@name", fig.Name));
            com.Parameters.Add(new SqlParameter("@scale", fig.Scale));
            com.Parameters.Add(new SqlParameter("@price", fig.Price));
            com.Parameters.Add(new SqlParameter("@info", fig.Info));
            com.Parameters.Add(new SqlParameter("@quantity", fig.Quantity));
            com.Parameters.Add(new SqlParameter("@image", fig.Image));
            com.Parameters.Add(new SqlParameter("@manufacturer", fig.Manufacturer));
            return com.ExecuteNonQuery() > 0;
        }

        public bool DeleteFigure(Figure fig)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "DELETE FROM FIGURE WHERE Code = @Code";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@Code", fig.Code));
            return com.ExecuteNonQuery() > 0;
        }

        public List<Figure> SearchFigure(String info, String colName)
        {
            try
            {
            List<Figure> figures = new List<Figure>();

            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT * FROM FIGURE WHERE " + colName + " LIKE '%" + info + "%'";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Figure fig = new Figure()
                {
                    Code = (int)dr["code"],
                    Name = (String)dr["name"],
                    Scale = (String)dr["scale"],
                    Price = (int)dr["price"],
                    Info = (String)dr["info"],
                    Quantity = (int)dr["quantity"],
                    Image = (String)dr["image"],
                    Manufacturer = (String)dr["manufacturer"],
                };
                figures.Add(fig);
            }
            return figures;
            }
            catch
            {
                return null;
            }
        }

        public List<Figure> SelectFigureByDecendingPrice()
        {
            try
            {
            List<Figure> figures = new List<Figure>();

            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT * FROM FIGURE ORDER BY PRICE DESC";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Figure fig = new Figure()
                {
                    Code = (int)dr["code"],
                    Name = (String)dr["name"],
                    Scale = (String)dr["scale"],
                    Price = (int)dr["price"],
                    Info = (String)dr["info"],
                    Quantity = (int)dr["quantity"],
                    Image = (String)dr["image"],
                    Manufacturer = (String)dr["manufacturer"],
                };
                figures.Add(fig);
            }
            return figures;
            }
            catch
            {
                return null;
            }
        }

        public List<Figure> SelectFigureByAscendingPrice()
        {
            try
            {
            List<Figure> figures = new List<Figure>();

            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT * FROM FIGURE ORDER BY PRICE ASC";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Figure fig = new Figure()
                {
                    Code = (int)dr["code"],
                    Name = (String)dr["name"],
                    Scale = (String)dr["scale"],
                    Price = (int)dr["price"],
                    Info = (String)dr["info"],
                    Quantity = (int)dr["quantity"],
                    Image = (String)dr["image"],
                    Manufacturer = (String)dr["manufacturer"],
                };
                figures.Add(fig);
            }
            return figures;
            }
            catch
            {
                return null;
            }
        }
    }
}