﻿using FigureService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FigureService.DAL
{
    public class DetailedOrderDAO
    {
        public bool InsertDetailedOrder(OrderDetailed orderDetailed)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "INSERT INTO ORDER_DETAILED VALUES( @orderID, @productID, @quantity, @price, @totalPrice)";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@orderID", orderDetailed.OrderId));
            com.Parameters.Add(new SqlParameter("@productID", orderDetailed.ProductId));
            com.Parameters.Add(new SqlParameter("@quantity", orderDetailed.Quantity));
            com.Parameters.Add(new SqlParameter("@price", orderDetailed.Price));
            com.Parameters.Add(new SqlParameter("@totalPrice", orderDetailed.TotalPrice));
            return com.ExecuteNonQuery() > 0;
        }

        public bool DeleteDetailedOrder(int orderId, int productId)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "DELETE ORDER_DETAILED WHERE order_ID = @order_ID AND product_ID = @product_ID";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@order_ID", orderId));
            com.Parameters.Add(new SqlParameter("@product_ID", productId));
            return com.ExecuteNonQuery() > 0;
        }

        public bool UpdateDetailedOrder(int orderId, int productId, int quantity, int totalPrice)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "UPDATE ORDER_DETAILED " +
                "SET quantity = @quantity, total_price = @total_price " +
                "WHERE order_ID = @order_ID AND product_ID = @product_ID";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@order_ID", orderId));
            com.Parameters.Add(new SqlParameter("@product_ID", productId));
            com.Parameters.Add(new SqlParameter("@quantity", quantity));
            com.Parameters.Add(new SqlParameter("@total_price", totalPrice));
            return com.ExecuteNonQuery() > 0;
        }


    }
}