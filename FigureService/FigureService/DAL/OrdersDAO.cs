﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FigureService.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace FigureService.DAL
{
    public class OrdersDAO
    {
        public List<Orders> SelectAllOrder()
        {
            try
            {
            List<Orders> orders = new List<Orders>();
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT * FROM ORDERS ORDER BY order_date desc";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Orders order = new Orders()
                {
                    Id = (int)dr["id"],
                    OrderDate = (DateTime)dr["order_date"],
                    State = (string)dr["state"],
                    Total = (int)dr["total"],
                    CustomerId = (int)dr["customer_id"],
                };
                orders.Add(order);
            }
            return orders;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateOrder(Orders orders)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "UPDATE ORDERS SET state = @state, total = @total WHERE id = @id";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@id", orders.Id));
            com.Parameters.Add(new SqlParameter("@state", orders.State));
            com.Parameters.Add(new SqlParameter("@total", orders.Total));
            return com.ExecuteNonQuery() > 0;
        }

        public int InsertOrder(Orders order)
        {
            int modifiedId = 0;
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "INSERT INTO ORDERS OUTPUT INSERTED.id VALUES(@order_date, @state, @total, @customer_id)";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@order_date", order.OrderDate));
            com.Parameters.Add(new SqlParameter("@state", order.State));
            com.Parameters.Add(new SqlParameter("@total", order.Total));
            com.Parameters.Add(new SqlParameter("@customer_id", order.CustomerId));
            modifiedId = (int)com.ExecuteScalar();
            return modifiedId;
        }
                       
        public List<Orders> SearchOrder(String info, String colName)
        {
            try
            {
            List<Orders> orders = new List<Orders>();

            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT * FROM ORDERS WHERE " + colName + " LIKE '%" + info + "%' ORDER BY order_date desc";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Orders order = new Orders()
                {
                    Id = (int)dr["id"],
                    OrderDate = (DateTime)dr["order_date"],
                    State = (string)dr["state"],
                    Total = (int)dr["total"],
                    CustomerId = (int)dr["customer_id"],
                };
                orders.Add(order);
            }
            return orders;
            }
            catch
            {
                return null;
            }
        }
    }
}