﻿using FigureService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FigureService.DAL
{
    public class ReceiptDAO
    {
        public List<Receipt> ShowOrderProductList(int ordersId)
        {
            try
            {
                List<Receipt> figOrdered = new List<Receipt>();
                String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
                SqlConnection con = new SqlConnection(strCon);
                con.Open();
                String strCom = "Select f.code, f.name, f.price, o.Quantity, f.quantity as max_quantity " +
                    "From Figure as f , ORDER_DETAILED as o , ORDERS " +
                    "Where o.product_id = f.code and o.Order_id = Orders.id and Orders.id = @id " +
                    "Order By f.code ASC";
                SqlCommand com = new SqlCommand(strCom, con);
                com.Parameters.Add(new SqlParameter("@id", ordersId));
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    Receipt receipt = new Receipt()
                    {
                    Code = (int)dr["code"],
                    Name = (String)dr["name"],
                    Price = (int)dr["price"],
                    Quantity = (int)dr["Quantity"],
                    Max_Quantity = (int)dr["max_quantity"]
                    };
                    figOrdered.Add(receipt);
                }
                return figOrdered;
            }
            catch
            {
                return null;
            }
        }
    }
}