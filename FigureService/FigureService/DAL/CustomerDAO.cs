﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FigureService.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace FigureService.DAL
{
    public class CustomerDAO
    {
        public List<Customer> SelectAllCustomer()
        {
            try
            {
            List<Customer> customers = new List<Customer>();
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT * FROM CUSTOMER";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Customer customer = new Customer()
                {
                    Id = (int)dr["id"],
                    Name = (String)dr["name"],
                    Address = (String)dr["address"],
                    PhoneNumber = (String)dr["phonenumber"],
                };
                customers.Add(customer);

            }
            return customers;
            }
            catch
            {
                return null;
            }
        }

        public bool InsertCustomer(Customer customer)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "INSERT INTO CUSTOMER VALUES(@name, @address, @phonenumber)";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@name", customer.Name));
            com.Parameters.Add(new SqlParameter("@address", customer.Address));
            com.Parameters.Add(new SqlParameter("@phonenumber", customer.PhoneNumber));
            return com.ExecuteNonQuery() > 0;
        }

        public bool UpdateCustomer(Customer customer)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "UPDATE CUSTOMER SET name = @name, address = @address, phonenumber = @phonenumber WHERE id = @id";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@id", customer.Id));
            com.Parameters.Add(new SqlParameter("@name", customer.Name));
            com.Parameters.Add(new SqlParameter("@address", customer.Address));
            com.Parameters.Add(new SqlParameter("@phonenumber", customer.PhoneNumber));
            return com.ExecuteNonQuery() > 0;
        }
      
        public bool DeleteCustomer(Customer customer)
        {
            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "DELETE FROM CUSTOMER WHERE id = @id";
            SqlCommand com = new SqlCommand(strCom, con);
            com.Parameters.Add(new SqlParameter("@id", customer.Id));
            return com.ExecuteNonQuery() > 0;
        }

        public List<Customer> SearchCustomer(String info, String colName)
        {
            try
            {
            List<Customer> customes = new List<Customer>();

            String strCon = ConfigurationManager.ConnectionStrings["strCon"].ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            con.Open();
            String strCom = "SELECT * FROM CUSTOMER WHERE " + colName + " LIKE '%" + info + "%'";
            SqlCommand com = new SqlCommand(strCom, con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Customer customer = new Customer()
                {
                    Id = (int)dr["id"],
                    Name = (String)dr["name"],
                    Address = (String)dr["address"],
                    PhoneNumber = (String)dr["phonenumber"],
                };
                customes.Add(customer);
            }
            return customes;
            }
            catch
            {
                return null;
            }
        }
    }
}