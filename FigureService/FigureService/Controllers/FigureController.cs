﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using FigureService.Models;
using FigureService.DAL;

namespace FigureService.Controllers
{
    public class FigureController : ApiController
    {
        [HttpGet]
        [Route("api/FigureService/GetAllFigure")]
        public List<Figure> GetAllFigure()
        {
            List<Figure> figures = new FigureDAO().SelectAllFigure();
            return figures;
        }

        [HttpPost]
        [Route("api/FigureService/AddFigure")]
        public bool AddFigure(Figure fig)
        {
            bool result = new FigureDAO().InsertFigure(fig);
            return result;
        }

        [HttpPost]
        [Route("api/FigureService/UpdateFigure")]
        public bool UpdateFigure(Figure fig)
        {
            bool result = new FigureDAO().UpdateFigure(fig);
            return result;
        }

        [HttpPost]
        [Route("api/FigureService/DeleteFigure")]
        public bool DeleteFigure(Figure fig)
        {
            bool result = new FigureDAO().DeleteFigure(fig);
            return result;
        }

        [HttpGet]
        [Route("api/FigureService/SearchFigure")]
        public List<Figure> SearchFigure(String info, String colName)
        {
            List<Figure> figures = new FigureDAO().SearchFigure(info, colName);
            return figures;
        }

        [HttpGet]
        [Route("api/FigureService/GetFigureByDescendingPrice")]
        public List<Figure> GetFigureByDescendingPrice()
        {
            List<Figure> figures = new FigureDAO().SelectFigureByDecendingPrice();
            return figures;
        }

        [HttpGet]
        [Route("api/FigureService/GetFigureByAscendingPrice")]
        public List<Figure> GetFigureByAscendingPrice()
        {
            List<Figure> figures = new FigureDAO().SelectFigureByAscendingPrice();
            return figures;
        }
    }
}