﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using FigureService.Models;
using FigureService.DAL;

namespace FigureService.Controllers
{
    public class OrderController : ApiController
    {
        [HttpGet]
        [Route("api/FigureService/GetAllOrder")]
        public List<Orders> GetAllOrder()
        {
            List<Orders> orders = new OrdersDAO().SelectAllOrder();
            return orders;
        }

        [HttpPost]
        [Route("api/FigureService/AddOrder")]
        public int AddOrder(Orders order)
        {
            int result = new OrdersDAO().InsertOrder(order);
            return result;
        }

        [HttpPost]
        [Route("api/FigureService/UpdateOrder")]
        public bool UpdateOrder(Orders orders)
        {
            bool result = new OrdersDAO().UpdateOrder(orders);
            return result;
        }

        [HttpGet]
        [Route("api/FigureService/SearchOrder")]
        public List<Orders> SearchOrder(String info, String colName)
        {
            List<Orders> orders = new OrdersDAO().SearchOrder(info, colName);
            return orders;
        }
    }
}