﻿using FigureService.DAL;
using FigureService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FigureService.Controllers
{
    public class DetailedOrderController: ApiController
    {
        [HttpPost]
        [Route("api/FigureService/InsertDetailedOrder")]
        public bool InsertDetailedOrder(OrderDetailed orderDetailed)
        {
            bool result = new DetailedOrderDAO().InsertDetailedOrder(orderDetailed);
            return result;
        }

        [HttpPost]
        [Route("api/FigureService/DeleteDetailedOrder")]
        public bool DeleteDetailedOrder(int orderId, int productId)
        {
            bool result = new DetailedOrderDAO().DeleteDetailedOrder(orderId, productId);
            return result;
        }

        [HttpPost]
        [Route("api/FigureService/UpdateDetailedOrder")]
        public bool UpdateDetailedOrder(int orderId, int productId, int quantity, int totalPrice)
        {
            bool result = new DetailedOrderDAO().UpdateDetailedOrder(orderId, productId, quantity, totalPrice);
            return result;
        }
    }
}