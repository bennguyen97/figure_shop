﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using FigureService.Models;
using FigureService.DAL;

namespace FigureService.Controllers 
{
    public class CustomerController : ApiController
    {
        [HttpGet]
        [Route("api/FigureService/GetAllCustomer")]
        public List<Customer> GetAllCustomer()
        {
            List<Customer> customers = new CustomerDAO().SelectAllCustomer();
            return customers;
        }

        [HttpPost]
        [Route("api/FigureService/AddCustomer")]
        public bool AddCustomer(Customer customer)
        {
            bool result = new CustomerDAO().InsertCustomer(customer);
            return result;
        }

        [HttpPost]
        [Route("api/FigureService/UpdateCustomer")]
        public bool UpdateCustomer(Customer customer)
        {
            bool result = new CustomerDAO().UpdateCustomer(customer);
            return result;
        }

        [HttpPost]
        [Route("api/FigureService/DeleteCustomer")]
        public bool DeleteCustomer(Customer customer)
        {
            bool result = new CustomerDAO().DeleteCustomer(customer);
            return result;
        }

        [HttpGet]
        [Route("api/FigureService/SearchCustomer")]
        public List<Customer> SearchCustomer(String info, String colName)
        {
            List<Customer> customers = new CustomerDAO().SearchCustomer(info, colName);
            return customers;
        }
    }
}