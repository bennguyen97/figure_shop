﻿using FigureService.DAL;
using FigureService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FigureService.Controllers
{
    public class ReceiptController : ApiController
    {
        [HttpGet]
        [Route("api/FigureService/ShowOrderProductList")]
        public List<Receipt> ShowOrderProductList(int ordersId)
        {
            List<Receipt> receipts = new ReceiptDAO().ShowOrderProductList(ordersId);
            return receipts;
        }
    }
}