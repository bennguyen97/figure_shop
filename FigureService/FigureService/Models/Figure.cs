﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FigureService.Models
{
    public class Figure
    {
        public int Code { get; set; }
        public string Name { get; set; }
        public string Scale { get; set; }
        public int Price { get; set; }
        public string Info { get; set; }
        public int Quantity { get; set; }   
        public string Image { get; set; }
        public string Manufacturer { get; set; }     
    }
}