﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FigureService.Models
{
    public class Receipt
    {

        public int Code { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public int Max_Quantity { get; set; }
    }
}